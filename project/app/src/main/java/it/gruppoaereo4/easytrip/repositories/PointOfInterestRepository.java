package it.gruppoaereo4.easytrip.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import it.gruppoaereo4.easytrip.model.NearbyPlace;
import it.gruppoaereo4.easytrip.model.Resource;
import it.gruppoaereo4.easytrip.model.pointofinterest.PhotoImageReference;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;
import it.gruppoaereo4.easytrip.model.PointOfInterestAPIResponse;
import it.gruppoaereo4.easytrip.services.PointOfInterestService;
import it.gruppoaereo4.easytrip.utils.ApiConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PointOfInterestRepository {

    private static final String TAG = "PointOfInterestRepo";

    private static PointOfInterestRepository instance;
    private PointOfInterestService pointOfInterestService;
    private PointOfInterest pointOfInterest;

    private PointOfInterestRepository(){
        pointOfInterestService = new Retrofit.Builder()
                .baseUrl(ApiConstants.PLACES_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PointOfInterestService.class);
    }

    public static synchronized PointOfInterestRepository getInstance(){
        if(instance == null){
            instance = new PointOfInterestRepository();
        }
        return instance;
    }

    public void getPointOfInterest(MutableLiveData<PointOfInterest> poi, String placeId){
        Log.d(TAG,"getPointOfInterest: call");
        Log.d(TAG,placeId);
        Call<PointOfInterestAPIResponse> call = pointOfInterestService.getPlaceByID(
                placeId,
                PointOfInterest.getFieldsApiCall(),
                ApiConstants.Keys.GOOGLE_MAPS_API_KEY,
                ApiConstants.PLACES_LANGUAGE_IT);

        Log.d(TAG,"getPointOfInterest: placeService created");

        call.enqueue(new Callback<PointOfInterestAPIResponse>() {
            @Override
            public void onResponse(Call<PointOfInterestAPIResponse> call, Response<PointOfInterestAPIResponse> response) {
                Log.d(TAG,"getPointOfInterest - placeService onResponse: response status "+response.body().getStatus());
                Log.d(TAG,"getPointOfInterest - placeService onResponse: statusCode "+response.raw().code());
                Log.d(TAG,"getPointOfInterest - placeService onResponse: URL " + response.raw().request().url().toString());

                pointOfInterest = response.body().getPointsOfInterest();
                if(pointOfInterest == null) {
                    poi.postValue(null);
                } else {
                    //Sets the place id from the gmap because the API return a different value for the id
                    pointOfInterest.setPlaceID(placeId);
                    getPhoto(poi, pointOfInterest);
                }
            }

            @Override
            public void onFailure(Call<PointOfInterestAPIResponse> call, Throwable t) {
                Log.d(TAG,"getPointOfInterest - placeService onFailure: request KO: "+t.getMessage());
                poi.postValue(null);
            }
        });
    }

    private void getPhoto(MutableLiveData<PointOfInterest> poi, PointOfInterest pointOfInterest){
        if(pointOfInterest.getPhotos() != null && pointOfInterest.getPhotos().size() > 0) {
            /*String imageRequestUrlPlaceholder = ApiConstants.PLACES_API_BASE_URL +
                    "photo?maxwidth=%d&maxheight=%d&photoreference=%s&key=%s";
            String imageRequestUrl = String.format(imageRequestUrlPlaceholder,
                    800, 600,
                    pointOfInterest.getPhotos().get(0).getPhotoReference(),
                    ApiConstants.Keys.GOOGLE_MAPS_API_KEY);*/
            String imageRequestUrl = PhotoImageReference.generateImageRequestURL(
                    800,600, pointOfInterest.getPhotos().get(0).getPhotoReference());
            
            Log.d(TAG, "getPhoto: photo request URL "+imageRequestUrl);
            pointOfInterest.setImageUrl(imageRequestUrl);
        }
        poi.setValue(pointOfInterest);
    }

    public void getPointOfInterests(MutableLiveData<List<PointOfInterest>> poi, Set<String> placeIDs){

        List<PointOfInterest> pois = new ArrayList<PointOfInterest>();
        final int[] counter = {0};

        for(int i = 0; i < placeIDs.size(); i++){
            String placeId = placeIDs.toArray()[i].toString();
            Log.d(TAG,"getPointOfInterests: call");
            Log.d(TAG,placeId);
            Call<PointOfInterestAPIResponse> call = pointOfInterestService.getPlaceByID(
                    placeId,
                    PointOfInterest.getFieldsApiCall(),
                    ApiConstants.Keys.GOOGLE_MAPS_API_KEY,
                    ApiConstants.PLACES_LANGUAGE_IT);

            Log.d(TAG,"getPointOfInterests: placeService created");
            call.enqueue(new Callback<PointOfInterestAPIResponse>() {
                @Override
                public void onResponse(Call<PointOfInterestAPIResponse> call, Response<PointOfInterestAPIResponse> response) {
                    counter[0]++;
                    Log.d(TAG,"getPointOfInterests - placeService onResponse: response status "+response.body().getStatus());
                    Log.d(TAG,"getPointOfInterests - placeService onResponse: statusCode "+response.raw().code());
                    Log.d(TAG,"getPointOfInterests - placeService onResponse: URL " + response.raw().request().url().toString());

                    pointOfInterest = response.body().getPointsOfInterest();
                    if(pointOfInterest != null){
                        pointOfInterest.setPlaceID(placeId);
                        if(pointOfInterest.getPhotos() != null && pointOfInterest.getPhotos().size() > 0) {
                            String imageRequestUrl = PhotoImageReference.generateImageRequestURL(
                                    800,600,
                                    pointOfInterest.getPhotos().get(0).getPhotoReference());
                            /*
                            String imageRequestUrlPlaceholder = ApiConstants.PLACES_API_BASE_URL +
                                    "photo?maxwidth=%d&maxheight=%d&photoreference=%s&key=%s";
                            String imageRequestUrl = String.format(imageRequestUrlPlaceholder,
                                    800, 600,
                                    pointOfInterest.getPhotos().get(0).getPhotoReference(),
                                    ApiConstants.Keys.GOOGLE_MAPS_API_KEY);*/
                            Log.d(TAG, "getPhoto: photo request URL "+imageRequestUrl);
                            pointOfInterest.setImageUrl(imageRequestUrl);
                        }
                        pois.add(pointOfInterest);
                        Log.d(TAG,"List: "+pois.toString());
                        checkCounter(poi,placeIDs.size(),counter[0],pois);
                    }
                }

                @Override
                public void onFailure(Call<PointOfInterestAPIResponse> call, Throwable t) {
                    counter[0]++;
                    Log.d(TAG,"getPointOfInterests - placeService onFailure: request KO: "+t.getMessage());
                    pois.add(null);
                    checkCounter(poi,placeIDs.size(),counter[0],pois);
                }
            });

        }



    }

    private void checkCounter(MutableLiveData<List<PointOfInterest>> poi, int length,int counter, List<PointOfInterest> pois){
        if(length==counter){
            Log.d(TAG, "poi: "+poi.toString());
            Log.d(TAG, "length: "+length);
            Log.d(TAG, "counter: "+counter);
            Log.d(TAG, "pois: "+pois.toString());
            poi.postValue(pois);
        }
    }

}