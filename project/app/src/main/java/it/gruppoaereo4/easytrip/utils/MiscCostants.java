package it.gruppoaereo4.easytrip.utils;

public class MiscCostants {
    public static final String INTENT_POI_KEY = "POINT_OF_INTEREST_KEY";
    public static final String DATE_FORMAT = "dd/MM/yy";
}
