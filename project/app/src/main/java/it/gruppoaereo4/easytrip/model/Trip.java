package it.gruppoaereo4.easytrip.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;

public class Trip implements Parcelable {

    private String ID;
    private String name;
    private Set<String> pointOfInterestIDSet;
    private Date startDate;
    private Date endDate;
    private String imageUrl = "";

    public Trip(String ID, String name, Set<String> pointOfInterestIDSet, Date startDate, Date endDate) {
        this.ID = ID;
        this.name = name;
        this.pointOfInterestIDSet = pointOfInterestIDSet;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Trip(String name, Date startDate, Date endDate){
        this.ID = generateID();
        this.name = name;
        this.pointOfInterestIDSet = new HashSet<>();
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Set<String> getPointOfInterestIDSet() {
        return pointOfInterestIDSet;
    }

    public Trip(String name) {
        this.name = name;
        this.ID = generateID();
        this.pointOfInterestIDSet = new HashSet<>();
    }

    // Generate an ID for a new Trip
    private String generateID() {
        return new java.text.SimpleDateFormat("yyyyMMddHHmmssSS").format(new java.util.Date());
    }

    public void addPointOfInterest(PointOfInterest pointOfInterest){
        pointOfInterestIDSet.add(pointOfInterest.getPlaceID());
    }

    public void removePointOfInterest(PointOfInterest pointOfInterest){
        pointOfInterestIDSet.remove(pointOfInterest.getPlaceID());
    }

    public void addPointOfInterest(PointOfInterest... pointsOfInterest){
        for(PointOfInterest point : pointsOfInterest)
            addPointOfInterest(point);
    }

    public void removePointOfInterest(PointOfInterest... pointsOfInterest){
        for(PointOfInterest point : pointsOfInterest)
            removePointOfInterest(point);
    }

    protected Trip(Parcel in) {
        ID = in.readString();
        name = in.readString();
        pointOfInterestIDSet = (Set) in.readValue(Set.class.getClassLoader());
        long tmpStartDate = in.readLong();
        startDate = tmpStartDate != -1 ? new Date(tmpStartDate) : null;
        long tmpEndDate = in.readLong();
        endDate = tmpEndDate != -1 ? new Date(tmpEndDate) : null;
        imageUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeString(name);
        dest.writeValue(pointOfInterestIDSet);
        dest.writeLong(startDate != null ? startDate.getTime() : -1L);
        dest.writeLong(endDate != null ? endDate.getTime() : -1L);
        dest.writeString(imageUrl);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Trip> CREATOR = new Parcelable.Creator<Trip>() {
        @Override
        public Trip createFromParcel(Parcel in) {
            return new Trip(in);
        }

        @Override
        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };
}
