package it.gruppoaereo4.easytrip.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import it.gruppoaereo4.easytrip.services.DescriptionService;
import it.gruppoaereo4.easytrip.utils.ApiConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DescriptionRepository {

    private static DescriptionRepository instance;
    private DescriptionService descriptionService;

    private static final String TAG = "DescriptionRepository";

    private DescriptionRepository(){
        descriptionService = new Retrofit.Builder()
                .baseUrl(ApiConstants.WIKIMEDIA_IT_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(DescriptionService.class);
    }

    public static synchronized DescriptionRepository getInstance(){
        if(instance == null){
            instance = new DescriptionRepository();
        }
        return instance;
    }

    public void getDescription(MutableLiveData<String> description, String poiName){
        Call<JsonElement> call = descriptionService.getDescription(poiName);

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                //parsing string response to json object
                JsonObject pages = response.body()
                        .getAsJsonObject().get("query")
                        .getAsJsonObject().get("pages")
                        .getAsJsonObject();

                //Get the first value on the list and extract the description
                JsonObject descJsonElement = pages.entrySet().iterator().next().getValue()
                        .getAsJsonObject();

                 String desc = descJsonElement.has("extract") ?
                         descJsonElement.get("extract").getAsString() :
                         "";

                Log.d(TAG, "OnResponse: "+response.raw().request().url().toString());
                description.postValue(desc);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.d(TAG, "OnResponse: failed");
            }
        });

    }

}
