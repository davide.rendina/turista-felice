package it.gruppoaereo4.easytrip;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import it.gruppoaereo4.easytrip.databinding.FragmentMapBinding;
import it.gruppoaereo4.easytrip.model.NearbyPlace;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;
import it.gruppoaereo4.easytrip.utils.ApiConstants;
import it.gruppoaereo4.easytrip.utils.MiscCostants;
import it.gruppoaereo4.easytrip.viewmodels.NearbySearchViewModel;
import it.gruppoaereo4.easytrip.viewmodels.PointOfInterestViewModel;


public class MapFragment extends Fragment {
    // Values for request
    private final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private final int ENABLE_GPS_REQUEST_CODE = 2;
    // Key for savedInstanceState
    private final String CARDVISIBLE_BUNDLEKEY = "CARD_VISIBLE";
    private final String ACTUALPOIID_BUNDLEKEY = "ACTUAL_POI_ID";

    private final String TAG = "MapFragment";
    private final float DEFAULT_ZOOM = 18.5f;
    private final float NEARBY_SEARCH_ZOOM = 15.5f;

    private FragmentMapBinding binding;
    private GoogleMap gMap;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private boolean needStartMoveCurrentLocation = false;
    private boolean isCardVisible = false;
    // Represent the poi which is showed in the card
    private PointOfInterest actualPOI = null;

    private PointOfInterestViewModel pointOfInterestViewModel;
    // Attributes for handling nearby search with filters
    private NearbySearchViewModel nearbySearchViewModel;
    private List<Marker> nearbyMarkers;
    private Map<String, String> markerPlaceIdMap;
    // Represent a pending request, generated when a SHOW_POINT_OF_INTEREST request is made but the map is not ready yet
    private LatLng pendingPoiLocation;
    private String pendingPoiID;

    private LocationRequest mLocationRequest;

    // Class which contains all the key and values for making specific request from other activities to the map
    public static class MAP_REQUEST {
        // Define the type oh the search to perform on the map
        public static final String TYPE_KEY="search_request_type";
        public enum TYPE_VALUES{
            SHOW_POINT_OF_INTEREST, NEARBY_SEARCH
        }
        //Define the keys for passing place id and position of a single place
        public static final String POI_LATLNG_KEY = "latlng_location_search";
        public static final String POI_ID_KEY = "id_location_search";
        //Define the available categories and the key in order to perform a nearby search
        public static final String CATEGORY_NEARBY_KEY = "type_nearby_search";
        public enum CATEGORY_NEARBY_VALUES{
            MUSEUM, ATTRACTION, BARANDPUB, RESTAURANT, HOTEL, BANCOMAT
        }
    }

    private MutableLiveData<LatLng> location;

    private Observer<LatLng> myLocationObserver = latLng -> {
        Log.d(TAG, "My location observer");
        activeMapLocationPoint();
        moveMapToLocation(latLng, DEFAULT_ZOOM);
    };

    private Observer<LatLng> nearbyPlacesObserver = latLng -> {
        Log.d(TAG, "nearby places observer");

        if(latLng != null) {
            String type = getSelectedChipPlaceType();
            nearbySearchViewModel.getNearbyPlaces(latLng.latitude, latLng.longitude, 500, type.toLowerCase())
                    .observe(getActivity(), listResource -> {
                        // Clean the current Markers
                        destroyMarkers();

                        // Add markers to the map
                        List<NearbyPlace> nearbyPlaces = listResource.getData();
                        for (NearbyPlace place : nearbyPlaces) {
                            float[] hsv = new float[3];
                            ColorUtils.colorToHSL(getResources().getColor(R.color.colorAccent), hsv);
                            Marker m = gMap.addMarker(new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.defaultMarker(hsv[0]))
                                    .position(new LatLng(place.getGeometry().getLocation().getLat(),
                                            place.getGeometry().getLocation().getLng())));
                            nearbyMarkers.add(m);
                            markerPlaceIdMap.put(m.getId(), place.getId());
                        }
                        // Animate the camera
                        activeMapLocationPoint();
                        moveMapToLocation(latLng, NEARBY_SEARCH_ZOOM);
                        //gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, NEARBY_SEARCH_ZOOM));
                    });
        }
        else{
            //binding.filtersChipGroup.clearCheck();
            Snackbar.make(this.getView(), R.string.map_fragment_location_fail_msg, Snackbar.LENGTH_SHORT);
        }
    };


    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "status: createView");
        binding = FragmentMapBinding.inflate(inflater, container, false);
        final View view = binding.getRoot();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());

        //Set the map
        initializeMap();
        initializeFilters();
        nearbyMarkers = new LinkedList<>();
        markerPlaceIdMap = new HashMap<>();

        // Add behavior to POI card
        setPointOfInterestCardBehavior(view);

        //Set view model
        pointOfInterestViewModel = new PointOfInterestViewModel();
        nearbySearchViewModel = new NearbySearchViewModel();

         //Set myLocation image button
         binding.myLocationImageButton.setOnClickListener(view1 -> {
             needStartMoveCurrentLocation = true;
             requireLocation(myLocationObserver);
         });

         //Sets autocomplete search
        initializeAutocompleteSearchFragment();

        // Check if there's a saved state
        if(savedInstanceState != null){
            Log.d(TAG, "onCreate: savedInstanceState found");
            String actualPOIID = savedInstanceState.getString(ACTUALPOIID_BUNDLEKEY);
            Boolean isCardVisibleBundle = savedInstanceState.getBoolean(CARDVISIBLE_BUNDLEKEY);

            if(! actualPOIID.equals("") && isCardVisibleBundle){
                getPointOfInterest(actualPOIID);
                isCardVisible = isCardVisibleBundle;
            }
        }

        // Check if a request has been made to this fragment
        Bundle mapRequest = this.getArguments();
        needStartMoveCurrentLocation = savedInstanceState == null && mapRequest == null;
        if (mapRequest != null)
            handleSearchRequest(mapRequest);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "Status: start");
        if (gMap != null) {
            // If the activity was only started and when it stopped a card was visible, restore the state
            if(actualPOI != null && isCardVisible || ! nearbyMarkers.isEmpty()){
                if(actualPOI != null)
                    getPointOfInterest(actualPOI.getPlaceID());
                needStartMoveCurrentLocation = false;
            }
        }
    }

    @Override
    public void onPause() {
        Log.d(TAG,"Status: pause");
        super.onPause();
        // If the activity is paused, disable the location
        disableLocation();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onSaveInstanceState");
        // Save if the card is visible and the shown poi, in order to restore it
        savedInstanceState.putBoolean(CARDVISIBLE_BUNDLEKEY, isCardVisible);
        savedInstanceState.putString(ACTUALPOIID_BUNDLEKEY,
                actualPOI == null ? "" : actualPOI.getPlaceID());
        super.onSaveInstanceState(savedInstanceState);
    }


    // Require the map and initialize it
    private void initializeMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(googleMap -> {
            gMap = googleMap;
            Log.d(TAG, "initializeMap: map ready");

            if(needStartMoveCurrentLocation)
                requireLocation(myLocationObserver);

            gMap.setOnPoiClickListener(pointOfInterest -> {
                Log.d(TAG, "onPoiClickListener: click on POI " +pointOfInterest.placeId);
                getPointOfInterest(pointOfInterest.placeId);
            });

            gMap.setOnMapClickListener(latLng -> {
                isCardVisible = false;
                hideOrRevealPOICard(isCardVisible);
                actualPOI = null;
            });

            gMap.setOnMarkerClickListener(marker -> {
                getPointOfInterest(markerPlaceIdMap.get(marker.getId()));
                Log.d(TAG, "Marker click " + marker.getId());
                return false;
            });

            //If there is a pending poi to be shown, handle it
            if(pendingPoiID != null &&  pendingPoiLocation!= null){
                getPointOfInterest(pendingPoiID);
                gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pendingPoiLocation, DEFAULT_ZOOM));
                pendingPoiID = null;
                pendingPoiLocation= null;
            }
        });
    }

    // Initialize the Places Autocomplete fragment
    private void initializeAutocompleteSearchFragment() {
        // Initialize place API
        if (!Places.isInitialized()) {
            Places.initialize(getActivity().getApplicationContext(), ApiConstants.Keys.GOOGLE_MAPS_API_KEY);
        }

        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(
                Place.Field.ID, Place.Field.LAT_LNG));

        autocompleteFragment.getView().setBackgroundColor(Color.WHITE);

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.d(TAG, "Place selected "+place.getId());
                getPointOfInterest(place.getId());
                gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), DEFAULT_ZOOM));
            }

            @Override
            public void onError(Status status) {
                Log.i(TAG, "An error occurred: " + status);
            }
        });
    }

    // Add behavior to Chip filters
    private void initializeFilters() {
        binding.filtersChipGroup.setOnCheckedChangeListener((group, checkedId) -> {
            destroyMarkers();
            if(isCardVisible) {
                isCardVisible = false;
                hideOrRevealPOICard(isCardVisible);
            }
            requireLocation(nearbyPlacesObserver);
        });
    }

    private String getSelectedChipPlaceType(){
        Place.Type requestType = null;
        int checkedId = binding.filtersChipGroup.getCheckedChipId();
        if(checkedId != -1) {
            if (checkedId == binding.chipAtm.getId()) requestType = Place.Type.ATM;
            if (checkedId == binding.chipBarpub.getId()) requestType = Place.Type.BAR;
            if (checkedId == binding.chipAttraction.getId()) requestType = Place.Type.TOURIST_ATTRACTION;
            if (checkedId == binding.chipHotel.getId()) requestType = Place.Type.LODGING;
            if (checkedId == binding.chipMuseum.getId()) requestType = Place.Type.MUSEUM;
            if (checkedId == binding.chipRestaurant.getId()) requestType = Place.Type.RESTAURANT;

            return requestType.toString();
        }
        return "";
    }

    // Destroy all the markers actually visible on the map
    private void destroyMarkers(){
        if(nearbyMarkers != null){
            for(Marker m : nearbyMarkers)
                m.remove();
            nearbyMarkers.clear();
            markerPlaceIdMap.clear();
        }
    }

    // Add behavior to point of interesst card
    private void setPointOfInterestCardBehavior(View view) {
        // Card click
        view.findViewById(R.id.poi_card).setOnClickListener(v -> {
            Intent startPoiActivityIntent = new Intent(getContext(), PointOfInterestActivity.class);
            // Set animation
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                    getActivity(),
                    Pair.create(
                            getActivity().findViewById(R.id.imageView_card_pointofinterest),
                            getString(R.string.pointofinterest_sharedElementName_poiImage)),
                    Pair.create(
                            getActivity().findViewById(R.id.title_textView),
                            getString(R.string.pointofinterest_sharedElementName_title)),
                    Pair.create(
                            getActivity().findViewById(R.id.time_textView),
                            getString(R.string.pointofinterest_sharedElementName_time)),
                    Pair.create(
                            getActivity().findViewById(R.id.type_textView),
                            getString(R.string.pointofinterest_sharedElementName_type)
                    )
            );
            startPoiActivityIntent.putExtra(MiscCostants.INTENT_POI_KEY, actualPOI);
            startActivity(startPoiActivityIntent, options.toBundle());
        });
    }


    // Call the Places API in order to retrieve the point of interest and show if the operation is successful
    private void getPointOfInterest(String placeId){
        Log.d(TAG,"getPointOfInterestAPI: call");
        pointOfInterestViewModel.getPointOfInterest(placeId).observe(getActivity(), pointOfInterest -> {
            Log.d(TAG,"pointOfInterestObserver: received API result");
            Log.d(TAG,"pointOfInterestObserver: received ID: " + pointOfInterest.getPlaceID());

            if(pointOfInterest == null){
                Snackbar.make(getActivity().findViewById(R.id.mainLayout),
                        getString(R.string.map_fragment_poinotfound),
                        Snackbar.LENGTH_LONG)
                        .show();
            } else {
                actualPOI = pointOfInterest;
                isCardVisible = true;
                updatePointOfInterestCardUI(pointOfInterest);
            }
        });
    }


    // Update UI to show the current point of interest
    private void updatePointOfInterestCardUI(PointOfInterest pointOfInterest){
        String msg = pointOfInterest == null ? "null" : pointOfInterest.getPlaceID();
        Log.d(TAG,"Set cardInfo of POI: "+msg);

        if(pointOfInterest != null) {

            binding.poiCard.titleTextView.setText(pointOfInterest.getName());
            binding.poiCard.typeTextView.setText(pointOfInterest.getTypeResourceID());

            //Set poin of interest time
            PointOfInterest.BusinnessStatus businnessStatus = pointOfInterest.getBusinessStatus();
            String timeText;
            switch (businnessStatus){
                case OPERATIONAL:
                    timeText = pointOfInterest.getTodayTime() == null ?
                            getString(R.string.poi_time_not_available) :
                            pointOfInterest.getTodayTime();
                    break;
                case CLOSED_TEMPORARILY:
                    timeText = getString(R.string.poi_time_closed_temporarily);
                    break;
                case CLOSED_PERMANENTLY:
                    timeText = getString(R.string.poi_time_closed_permanently);
                    break;
                case NOT_SET:
                default:
                    timeText = getString(R.string.poi_time_not_available);
                    break;
            }
            binding.poiCard.timeTextView.setText(timeText);

            //Load image
            try{
                String imageUrl = pointOfInterest.getImageUrl();
                if(imageUrl == null){
                    binding.poiCard.imageViewCardPointofinterest.setImageResource(R.drawable.image_not_found);
                    binding.poiCard.imageViewCardPointofinterest.invalidate();
                } else {
                    Picasso.get().load(imageUrl).noFade().into(binding.poiCard.imageViewCardPointofinterest, new Callback() {
                        @Override
                        public void onSuccess() {
                            binding.poiCard.imageViewCardPointofinterest.invalidate();
                            isCardVisible = true;
                            hideOrRevealPOICard(isCardVisible);
                        }
                        @Override
                        public void onError(Exception e) {
                            Log.d(TAG, "Picasso onError: "+e.getMessage());
                            binding.poiCard.imageViewCardPointofinterest.setImageResource(R.drawable.image_not_found);
                            isCardVisible = true;
                            hideOrRevealPOICard(isCardVisible);
                        }
                    });
                }
            }
            catch (Exception ex){
                binding.poiCard.imageViewCardPointofinterest.setImageResource(R.drawable.image_not_found);
            }


        }
    }

    // Show or reveal the point of interest card
    private void hideOrRevealPOICard(Boolean cardMustBeVisible){
        Log.d(TAG, "hideOrRevealCard: "+cardMustBeVisible);

        final CardView card = getActivity().findViewById(R.id.poi_card);
        float marginBottom = getContext().getResources().getDisplayMetrics().density * 8f;
        card.animate()
                .alpha(cardMustBeVisible ? 1.0f : 0.0f)
                .translationY(cardMustBeVisible ? -card.getHeight() - marginBottom : 0);
    }

    // Method to handle request from other fragments
    private void handleSearchRequest(Bundle mapRequest) {
        MAP_REQUEST.TYPE_VALUES requestType = MAP_REQUEST.TYPE_VALUES.valueOf(
                mapRequest.getString(MAP_REQUEST.TYPE_KEY, ""));

        if(requestType != null) {
            if (requestType == MAP_REQUEST.TYPE_VALUES.SHOW_POINT_OF_INTEREST) {
                LatLng placePosition = mapRequest.getParcelable(MAP_REQUEST.POI_LATLNG_KEY);
                String placeId = mapRequest.getString(MAP_REQUEST.POI_ID_KEY);

                // If the map is not ready keep the pending values , else show all on the map
                if(gMap != null){
                    getPointOfInterest(placeId);
                    gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(placePosition, DEFAULT_ZOOM));
                } else {
                    pendingPoiLocation = placePosition;
                    pendingPoiID = placeId;
                }
            }
            if (requestType == MAP_REQUEST.TYPE_VALUES.NEARBY_SEARCH) {
                MAP_REQUEST.CATEGORY_NEARBY_VALUES nearbySearchType = MAP_REQUEST.CATEGORY_NEARBY_VALUES.valueOf(
                        mapRequest.getString(MAP_REQUEST.CATEGORY_NEARBY_KEY));

                switch (nearbySearchType) {
                    case HOTEL:
                        binding.chipHotel.performClick();
                        break;
                    case MUSEUM:
                        binding.chipMuseum.performClick();
                        break;
                    case BANCOMAT:
                        binding.chipAtm.performClick();
                        break;
                    case BARANDPUB:
                        binding.chipBarpub.performClick();
                        break;
                    case ATTRACTION:
                        binding.chipAttraction.performClick();
                        break;
                    case RESTAURANT:
                        binding.chipRestaurant.performClick();
                        break;
                }
            }
        }
    }



    // Methods for handling the location

    // Try to enable the location with all the right permissions check
    private void requireLocation(Observer<LatLng> locationObserver){
        location = new MutableLiveData<>();
        location.observe(this, locationObserver);

        this.mLocationRequest = buildLocationRequest();

        Log.d(TAG, "enableLocation: call");
        if(isGoogleServicesOk()){
            checkLocationPermissions();
        }
    }

    private LocationRequest buildLocationRequest(){
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(1500);
        locationRequest.setFastestInterval(500);
        locationRequest.setNumUpdates(1);
        locationRequest.setSmallestDisplacement(10);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    private boolean isLocationPermissionGranted(){
        return ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void checkLocationPermissions(){
        Log.d(TAG,"Permission check");
        if (isLocationPermissionGranted()) {
            if(checkGPS()){
                getLastKnownLocation();
            }
            // Se il GPS non è enabled viene chiamato l'altro metodo
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this.getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new MaterialAlertDialogBuilder(getActivity())
                        .setTitle(getString(R.string.map_fragment_location_request_title))
                        .setMessage(getString(R.string.map_fragment_location_request_message))
                        .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                            requireLocationPermission();
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            } else {
                // Permission to access the location is missing. Show rationale and request permission
                requireLocationPermission();
            }
        }
    }

    // Show the dialog to accept permission
    private void requireLocationPermission(){
        requestPermissions(
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }

    // Returns True if connection with Google Services is ok
    private boolean isGoogleServicesOk(){
        Log.d(TAG,"Google services check");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getContext());
        if(available == ConnectionResult.SUCCESS)
            return true;
        else {
            Snackbar.make(getView(), R.string.location_google_service_error, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    // Returns True if GPS is enabled
    private boolean isGpsEnabled(){
        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    // If the GPS is enabled return true else request the activation
    private boolean checkGPS(){
        if(isGpsEnabled())
            return true;

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(false);

        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(getContext())
                .checkLocationSettings(builder.build());

        task.addOnFailureListener(getActivity(), exception -> {
            Log.d(TAG, "isGpsEnabled - onFailureListener: exception "+exception.getMessage());

            if (exception instanceof ResolvableApiException) {
                try {
                    ResolvableApiException resolvable = (ResolvableApiException) exception;
                    startIntentSenderForResult(
                            resolvable.getResolution().getIntentSender(),
                            ENABLE_GPS_REQUEST_CODE,
                            null, 0,0 ,0,null);
                } catch (IntentSender.SendIntentException sendEx) {}
            }
        });
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d(TAG, "onActivityResult: request "+requestCode+", result "+resultCode);
        super.onActivityResult(requestCode, resultCode, data);

        // If the GPS is enabled, show the position on the map
        if(resultCode == Activity.RESULT_OK) {
            if (requestCode == ENABLE_GPS_REQUEST_CODE) {
                getLastKnownLocation();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            // If the user grant the locatino permission and GPS is enabled, retrieve the position
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if(checkGPS()){
                    getLastKnownLocation();
                }
            }
        }
    }

    // Disable the location on the map
    private void disableLocation() {
        if(gMap != null)
            gMap.setMyLocationEnabled(false);
        Log.d(TAG, "Location disabled");
    }

    // Try to get the last location and shows it on the map
    private void getLastKnownLocation(){
        Log.d(TAG, "getLastKnownLocation: call");

        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                Location location = task.getResult();
                if(location != null)
                    this.location.postValue(new LatLng(location.getLatitude(), location.getLongitude()));
                else {
                    fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            if (locationResult != null){
                                Location resultLocation = locationResult.getLocations().get(0);
                                MapFragment.this.location.postValue(new LatLng(resultLocation.getLatitude(), resultLocation.getLongitude()));
                            }
                        }
                    }, Looper.getMainLooper());
                    //this.location.postValue(null);
                    //binding.myLocationImageButton.performClick();
                }
            }
        });


/*
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                Location location = task.getResult();
                if(location != null)
                    this.location.postValue(new LatLng(location.getLatitude(), location.getLongitude()));
                else {
                    this.location.postValue(null);
                    //binding.myLocationImageButton.performClick();
                }
            }
        });*/
    }

    // Animate the map in order to show the current user location
    private void moveMapToLocation(LatLng location, float zoom) {
        if(location != null)
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, zoom));
    }

    // Active location pin on the map
    private void activeMapLocationPoint(){
        if(gMap != null) {
            gMap.setMyLocationEnabled(true);
            gMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

}
