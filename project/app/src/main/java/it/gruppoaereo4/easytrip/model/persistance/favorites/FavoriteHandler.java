package it.gruppoaereo4.easytrip.model.persistance.favorites;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

import it.gruppoaereo4.easytrip.utils.SharedPreferencesKeys.FavoritesKeys;

public class FavoriteHandler {

    // The Set which contains all the favorites ID
    private Set<String> favoritesIDSet = new HashSet<>();
    private SharedPreferences sharedPreferences;
    private boolean modified = false;
    private Context context;

    private static FavoriteHandler instance;

    public static FavoriteHandler getInstance() {
        if(instance == null)
            instance = new FavoriteHandler();
        return instance;
    }

    public void init(Context context) {
        this.context = context;
        retrieveFavorites();
    }

    private FavoriteHandler(){}

    //Retrieve the favorites from the disk
    private void retrieveFavorites(){
        sharedPreferences = context.getSharedPreferences(FavoritesKeys.FILENAME, Context.MODE_PRIVATE);
        favoritesIDSet.addAll(sharedPreferences.getStringSet(FavoritesKeys.FAVORITE_LIST, new HashSet<>()));
    }

    public Set<String> getFavoritesIDSet() {
        return favoritesIDSet;
    }

    // Check if the given poi is a favorite
    public boolean isFavorite(String placeId){
        return favoritesIDSet.contains(placeId);
    }

    public void addFavorite(String placeId){
        favoritesIDSet.add(placeId);
        modified = true;
    }

    public void removeFavorite(String placeId){
        favoritesIDSet.remove(placeId);
        modified = true;
    }

    // If the given poi is a favorite, remove it from them and viceversa.
    // Return true if the point of interest is a favorite AFTER the execution of this method
    public boolean toggleFavorite(String placeId) {
        if(favoritesIDSet.contains(placeId)) {
            removeFavorite(placeId);
            return false;
        } else {
            addFavorite(placeId);
            return true;
        }
    }

    // Apply the changes at the end of the operations
    public void commitChanges(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(FavoritesKeys.FAVORITE_LIST, favoritesIDSet).apply();
    }

    // Return the number of favorite
    public int getNumberOfFavorites(){
        return favoritesIDSet.size();
    }

    // Return true if the favourite list was modified
    public boolean isModified(){
        return modified;
    }
}
