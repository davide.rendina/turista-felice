package it.gruppoaereo4.easytrip;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Set;

import it.gruppoaereo4.easytrip.adapters.FavouritesAdapter;
import it.gruppoaereo4.easytrip.databinding.FragmentFavouritesBinding;
import it.gruppoaereo4.easytrip.model.persistance.favorites.FavoriteHandler;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;
import it.gruppoaereo4.easytrip.utils.MiscCostants;
import it.gruppoaereo4.easytrip.viewmodels.FavouritesViewModel;

public class FavoritesFragment extends Fragment {

    private FragmentFavouritesBinding binding;
    private FavoriteHandler favoriteHandler;
    private FavouritesViewModel favouritesViewModel;
    private FavouritesAdapter favouritesAdapter;

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        binding = FragmentFavouritesBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        favouritesViewModel = new ViewModelProvider(requireActivity()).get(FavouritesViewModel.class);

        favoriteHandler = FavoriteHandler.getInstance();

        binding.toolbar2.setTitle(R.string.favorites_fragment_toolbar_title);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.favouritesFragmentRecyclerView.setLayoutManager(layoutManager);

        if(favoriteHandler.getFavoritesIDSet().size()>0) {

             favouritesAdapter = new FavouritesAdapter(getActivity(), getFavouriteList(favoriteHandler.getFavoritesIDSet()), new FavouritesAdapter.OnItemClickListener() {
                @Override
                public void onItemClickListener(PointOfInterest pointOfInterest) { //open Poi activity on click card
                    Intent startPoiActivityIntent = new Intent(getContext(), PointOfInterestActivity.class);
                    startPoiActivityIntent.putExtra(MiscCostants.INTENT_POI_KEY, pointOfInterest);
                    startActivityForResult(startPoiActivityIntent,1);
                }
            });


            final Observer<List<PointOfInterest>> observer = new Observer<List<PointOfInterest>>() {
                @Override
                public void onChanged(List<PointOfInterest> pointOfInterests) {
                    favouritesAdapter.setData(pointOfInterests);
                }
            };

            //recycler view with grid layout
            binding.favouritesFragmentRecyclerView.setAdapter(favouritesAdapter);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),2, GridLayoutManager.VERTICAL,false);
            binding.favouritesFragmentRecyclerView.setLayoutManager(gridLayoutManager);

            LiveData<List<PointOfInterest>> liveData = favouritesViewModel.getFavourites(favoriteHandler.getFavoritesIDSet());
            liveData.observe(this, observer);

        } else { //empty list
            binding.favouritesFragmentRecyclerView.setVisibility(View.INVISIBLE);
            binding.favouritesFragmentNoFavouritesTextView.setVisibility(View.VISIBLE);
        }
    }

    private List<PointOfInterest> getFavouriteList(Set<String> ids){
        List<PointOfInterest> favouriteList = favouritesViewModel.getFavourites(ids).getValue();
        if(favouriteList!=null)
            return favouriteList;
        else
            return null;
    }

   @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == 0) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(this).attach(this).commit();
            }
        }
    }
}