package it.gruppoaereo4.easytrip;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import it.gruppoaereo4.easytrip.databinding.ActivityUserProfileBinding;
import it.gruppoaereo4.easytrip.model.persistance.favorites.FavoriteHandler;
import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;
import it.gruppoaereo4.easytrip.viewmodels.FavouritesViewModel;

public class UserProfileActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private Toolbar toolbar;
    private ActivityUserProfileBinding binding;
    private float alphaDisabled = (float) 0.6;
    private FavoriteHandler favoriteHandler;
    private TripHandler tripHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUserProfileBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        firebaseAuth = FirebaseAuth.getInstance();
        setContentView(view);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        //number of trips and favourites
        favoriteHandler = FavoriteHandler.getInstance();
        binding.userProfileActivityNPreferitiTextView.setText(""+favoriteHandler.getNumberOfFavorites());
        tripHandler = TripHandler.getInstance();
        binding.userProfileActivityNViaggiTextView.setText(""+tripHandler.getNumberOfTrips());

        toolbar.setNavigationOnClickListener(button -> onBackPressed());

        binding.userProfileActivityChangePswTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(UserProfileActivity.this, ChangePasswordActivity.class);
                UserProfileActivity.this.startActivity(myIntent);
            }
        });

        binding.userProfileActivityContactTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(UserProfileActivity.this, ContactUsActivity.class);
                UserProfileActivity.this.startActivity(myIntent);
            }
        });

        //logout
        binding.userProfileActivityLogoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                updateUIUserNotLogged();
            }
        });

        //open settings activity
        binding.userProfileActivitySettingsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(UserProfileActivity.this, SettingsActivity.class);
                UserProfileActivity.this.startActivity(myIntent);
            }
        });

        //open login activity
        binding.userProfileActivityLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(UserProfileActivity.this, LoginActivity.class);
                UserProfileActivity.this.startActivity(myIntent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if(currentUser!=null) {
            updateUI(currentUser);
        } else {
            updateUIUserNotLogged();
        }
    }

    public void updateUI(FirebaseUser user){ //user logged
        binding.userProfileActivityEmailTextView.setText(user.getEmail());
        if(!user.getDisplayName().matches("")){
            binding.userProfileActivityName.setText(user.getDisplayName());
        } else {
           binding.userProfileActivityName.setText(user.getEmail());
        }
        if(user.getPhotoUrl()!=null){
            Picasso.get().load(user.getPhotoUrl()).into(binding.userProfileActivityPhoto);
        } else {
            binding.userProfileActivityPhoto.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.placeholder_profile,null));
            binding.userProfileActivityPhoto.setAlpha(alphaDisabled);
            if(!user.getDisplayName().matches("")) {
                binding.userProfileActivityInitial.setText(("" + user.getDisplayName().charAt(0)).toUpperCase());
            } else {
                binding.userProfileActivityInitial.setText(("" + user.getEmail().charAt(0)).toUpperCase());
            }
        }

        binding.userProfileActivityChangePswTextView.setVisibility(View.VISIBLE);
        binding.userProfileActivityLogoutTextView.setVisibility(View.VISIBLE);

    }

    public void updateUIUserNotLogged(){
        binding.userProfileActivityChangePswTextView.setEnabled(false);
        binding.userProfileActivityChangePswTextView.setVisibility(View.INVISIBLE);
        binding.userProfileActivityChangePswTextView.setAlpha(alphaDisabled);
        binding.userProfileActivityLogoutTextView.setVisibility(View.INVISIBLE);
        binding.userProfileActivityLogoutTextView.setEnabled(false);
        binding.userProfileActivityLogoutTextView.setAlpha(alphaDisabled);
        binding.userProfileActivityPhoto.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.placeholder_user,null));
        binding.userProfileActivityPhoto.setAlpha(alphaDisabled);
        binding.userProfileActivityInitial.setText("");
        binding.userProfileActivityName.setText("");
        binding.userProfileActivityEmailTextView.setText("");
        binding.userProfileActivityLoginButton.setVisibility((int)1);

    }
}
