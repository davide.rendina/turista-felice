package it.gruppoaereo4.easytrip.adapters;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import it.gruppoaereo4.easytrip.R;
import it.gruppoaereo4.easytrip.model.Trip;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;
import it.gruppoaereo4.easytrip.utils.MiscCostants;
import it.gruppoaereo4.easytrip.viewmodels.PointOfInterestViewModel;

public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.TripsViewHolder> {

    private Set<Trip> trips;
    private LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Context context;
    private LifecycleOwner lifecycleOwner;

    public interface OnItemClickListener {
        void onItemClick(Trip trip);
    }

    public TripsAdapter(Context context, LifecycleOwner lifecycleOwner, Set<Trip> trips, OnItemClickListener onItemClickListener) {
        this.layoutInflater = LayoutInflater.from(context);
        this.trips = trips;
        this.onItemClickListener = onItemClickListener;
        this.context = context;
        this.lifecycleOwner = lifecycleOwner;
    }

    @Override
    public TripsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.card_trip, parent, false);
        return new TripsViewHolder(view, context, lifecycleOwner);
    }

    @Override
    public void onBindViewHolder(TripsViewHolder holder, int position) {
        List<Trip> tripsList  = new ArrayList<Trip>(trips);
        holder.bind(tripsList.get(position), this.onItemClickListener);
    }

    public int getItemCount() {
        if (trips != null) {
            return trips.size();
        }
        return 0;
    }

    static class TripsViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewDate, textViewNumPlaces;
        ImageView imageViewPhoto;
        Context context;
        LifecycleOwner lifecycleOwner;

        TripsViewHolder(View view, Context context, LifecycleOwner lifecycleOwner) {
            super(view);
            this.context = context;
            this.lifecycleOwner = lifecycleOwner;
            textViewTitle = view.findViewById(R.id.titleFavouriteTextView);
            textViewDate = view.findViewById(R.id.dateTripTextView);
            textViewNumPlaces = view.findViewById(R.id.numPlacesTripTextView);
            imageViewPhoto = view.findViewById(R.id.photoTripImageView);
        }

        void bind(Trip trip, OnItemClickListener onItemClickListener) {
            List<String> pointOfInterests = new ArrayList<String>(trip.getPointOfInterestIDSet());
            textViewTitle.setText(trip.getName());
            textViewNumPlaces.setText(Integer.toString(pointOfInterests.size()));
            textViewDate.setText(new SimpleDateFormat(MiscCostants.DATE_FORMAT).format(trip.getStartDate())
                                    + " - " +
                                    new SimpleDateFormat(MiscCostants.DATE_FORMAT).format(trip.getEndDate()));

            //Prendo la foto di un place random dal trip e la uso come foto del trip
            if(pointOfInterests.size() > 0) {
                String randomPOI = pointOfInterests.get(new Random().nextInt(pointOfInterests.size()));
                final Observer<PointOfInterest> observer = new Observer<PointOfInterest>() {
                    @Override
                    public void onChanged(PointOfInterest poi) {
                        String imageUrl;
                        if(poi!=null) {
                            imageUrl = poi.getImageUrl();
                        } else {
                            imageUrl = null;
                        }
                        if (imageUrl == null) {
                            imageViewPhoto.setImageResource(R.drawable.image_not_found);
                            imageViewPhoto.invalidate();
                        } else {
                            trip.setImageUrl(imageUrl);
                            Picasso.get().load(imageUrl).noFade().into(imageViewPhoto, new Callback() {
                                @Override
                                public void onSuccess() {
                                    imageViewPhoto.invalidate();
                                }

                                @Override
                                public void onError(Exception e) {
                                    imageViewPhoto.setImageResource(R.drawable.image_not_found);
                                }
                            });
                        }
                    }
                };
                //Recupero il poi per prendere la foto
                LiveData<PointOfInterest> liveData = new PointOfInterestViewModel().getPointOfInterest(randomPOI);
                liveData.observe(lifecycleOwner, observer);
            } else {
                imageViewPhoto.setImageResource(R.drawable.image_not_found);
                imageViewPhoto.invalidate();
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(trip);
                }
            });
        }

    }

}
