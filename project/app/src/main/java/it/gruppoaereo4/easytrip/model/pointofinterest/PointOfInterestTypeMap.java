package it.gruppoaereo4.easytrip.model.pointofinterest;

import com.google.android.libraries.places.api.model.Place;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import it.gruppoaereo4.easytrip.R;
import it.gruppoaereo4.easytrip.utils.ApiConstants;

public class PointOfInterestTypeMap {
    Map<String, Integer> typesMap;

    private final String resourcePrefix = "pointofinterest_type_";
    private static PointOfInterestTypeMap instance;

    private static final int DEFAULT_RESOURCE = R.string.pointofinterest_type_general;

    private PointOfInterestTypeMap(){
        int resourceId;
        typesMap = new HashMap<>();

        for(Place.Type type : Place.Type.values()){
            String name = type.name().toLowerCase();
            try{
                Field resourceField = R.string.class.getDeclaredField(resourcePrefix+name);
                resourceId = resourceField.getInt(resourceField);
            } catch (NoSuchFieldException | IllegalAccessException ex){
                resourceId = R.string.pointofinterest_type_general;
            }

            typesMap.put(name, resourceId);
        }
    }

    public static PointOfInterestTypeMap getInstance() {
        if(instance==null)
            instance = new PointOfInterestTypeMap();
        return instance;
    }

    private int getStringResourceFromType(String value){
        if(typesMap.containsKey(value))
            return typesMap.get(value);
        else
            return DEFAULT_RESOURCE;
    }

    public int getStringResourceFromTypesArray(String[] types){
        if(types == null || types.length == 0)
            return DEFAULT_RESOURCE;

        String[] types1 = intersectArrays(types, ApiConstants.PLACES_TYPES1);
        if(types1.length == 0)
            return DEFAULT_RESOURCE;

        String type = types1.length == 1 ? types1[0] : types1[types1.length - 1];

        return getStringResourceFromType(type);
    }

    public String[] intersectArrays(String[] a, String[] b){
        Set<String> s1 = new HashSet<>(Arrays.asList(a));
        Set<String> s2 = new HashSet<>(Arrays.asList(b));
        s1.retainAll(s2);
        return s1.toArray(new String[s1.size()]);
    }
}
