package it.gruppoaereo4.easytrip.services;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DescriptionService {

    @GET("api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=")
    Call<JsonElement> getDescription(@Query("titles") String title);
}
