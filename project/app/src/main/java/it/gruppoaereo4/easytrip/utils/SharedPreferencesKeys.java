package it.gruppoaereo4.easytrip.utils;

public class SharedPreferencesKeys {

    public class FavoritesKeys {
        public static final String FILENAME = "favorites";
        public static final String FAVORITE_LIST = "favorite_list";
    }

    public class TripsKeys {
        public static final String TRIPLIST_FILENAME = "triplist";

        public static final String TRIP_NAME = "name";
        public static final String TRIP_ID = "id";
        public static final String TRIP_POI_LIST = "poi_id_list";
        public static final String TRIP_START_DATE = "start_date";
        public static final String TRIP_END_DATE = "end_date";
    }

}
