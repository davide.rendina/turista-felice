package it.gruppoaereo4.easytrip;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;

import it.gruppoaereo4.easytrip.dbsync.SyncHandler;
import it.gruppoaereo4.easytrip.model.persistance.favorites.FavoriteHandler;
import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    public static final String POI_EXTRA_KEY = "poi";
    private FirebaseAuth auth;
    private FavoriteHandler favoriteHandler;
    private TripHandler tripHandler;

    boolean noInternet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkInternetConnection();

        //Initialize handlers
        TripHandler.getInstance().init(getApplicationContext());
        FavoriteHandler.getInstance().init(getApplicationContext());

        auth = FirebaseAuth.getInstance();
        favoriteHandler = FavoriteHandler.getInstance();
        tripHandler = TripHandler.getInstance();

        //User logged
        if(isUserAuthenticated()) { //download favourite and trips from db
            SyncHandler.downloadData(getApplicationContext());
        }

        setUpNavigation();

        //Se viene passato il POI lo mostro sulla mappa
        if(getIntent().getExtras() != null) {
            PointOfInterest poi = getIntent().getExtras().getParcelable(POI_EXTRA_KEY);
            Bundle bundle = new Bundle();
            bundle.putString(MapFragment.MAP_REQUEST.TYPE_KEY, MapFragment.MAP_REQUEST.TYPE_VALUES.SHOW_POINT_OF_INTEREST.toString());
            bundle.putString(MapFragment.MAP_REQUEST.POI_ID_KEY, poi.getPlaceID());
            LatLng latLng = new LatLng(poi.getGeometry().getLocation().getLat(), poi.getGeometry().getLocation().getLng());
            bundle.putParcelable(MapFragment.MAP_REQUEST.POI_LATLNG_KEY, latLng);
            Navigation.findNavController(findViewById(R.id.nav_host_fragment)).navigate(R.id.action_homeFragment_to_mapFragment, bundle);
        }
    }

    private boolean needToModify() {
        return tripHandler.isModified() || favoriteHandler.isModified(); //if one of the lists was modified
    }

    private boolean checkInternetConnection() {
        //se non c'è connessione a internet mostro il dialog
        ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if(noInternet && isConnected) {
            finish();
            startActivity(getIntent());
        } else if(!isConnected) {
            noInternet = true;
            new AlertDialog.Builder(this).setMessage(R.string.title_no_internet_dialog)
                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            checkInternetConnection();
                        }
                    })
                    .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            System.exit(0);
                        }
                    })
                    .show();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //upload on db
        if(isUserAuthenticated() && needToModify()) { //upload only if one list was modified
            SyncHandler.uploadData(getApplicationContext());
        }
    }

    private boolean isUserAuthenticated(){ //check if the user is logged
        return auth!=null && auth.getCurrentUser()!=null;
    }

    public void setUpNavigation(){
        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_navigation);
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(bottomNavigation, navHostFragment.getNavController());
    }

}
