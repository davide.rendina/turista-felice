package it.gruppoaereo4.easytrip.dbsync;

import android.content.Context;
import android.util.ArraySet;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.gruppoaereo4.easytrip.model.Trip;
import it.gruppoaereo4.easytrip.model.persistance.favorites.FavoriteHandler;
import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;

public class DownloadWorker extends Worker {
    public DownloadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();

        FavoriteHandler favoriteHandler = FavoriteHandler.getInstance();
        TripHandler tripHandler = TripHandler.getInstance();

        syncFavouritesDownloadDB(auth.getUid(), db, favoriteHandler);
        syncTripsDownloadDB(auth.getUid(), db, tripHandler);

        return Result.success();
    }

    //Retrieve favourites from firebase
    public void syncFavouritesDownloadDB(String uID, FirebaseFirestore db, FavoriteHandler favoriteHandler) {

        db.collection("favourites")
                .document(uID)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            //Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                            mappingDownloadFavourites(document.getData(), favoriteHandler);
                        }/* else {
                            Log.d(TAG, "Nessun documento on questo uID");
                        }*/
                    } /*else {
                        Log.d(TAG, "Errore nel recupero dei preferiti:  ", task.getException());
                    }*/
                });
    }

    //Retrieve trips from firebase
    private void syncTripsDownloadDB(String uID, FirebaseFirestore db, TripHandler tripHandler) {
        Set<String> localTripsId = new ArraySet<>();
        Set<Trip> localTrips = tripHandler.getAllTrips();

        for (Trip currentTrip : localTrips) {
            localTripsId.add(currentTrip.getID()); //list of local trips
        }

        db.collection("trips")
                .document(uID)
                .collection("userTrips")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        for (QueryDocumentSnapshot doc : task.getResult()) {
                            mappingDownloadTrip(doc, localTripsId, tripHandler); //mapping current trip
                        }
                    }
                });

    }

    private void mappingDownloadTrip(QueryDocumentSnapshot documentSnapshot, Set<String> localTripsId, TripHandler tripHandler) {
        //Log.d(TAG, documentSnapshot.getId() + "-" + documentSnapshot.get("tripName"));
        Map<String, Object> tripMap = documentSnapshot.getData();

        //parameters used to create a trip object
        String tripId = documentSnapshot.getId();
        String tripName = (String) tripMap.get("tripName");
        Set<String> pois = new HashSet<>((List<String>) tripMap.get("poiIDs"));
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyy");
        String dateStart = (String) tripMap.get("tripStartDate");
        String dateEnd = (String) tripMap.get("tripsEndDate");
        Date tripStartDate = null;
        Date tripEndDate = null;

        try {
            tripEndDate = dateFormatter.parse(dateEnd);
            tripStartDate = dateFormatter.parse(dateStart);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Log.d(TAG, "StartDate:" + tripStartDate);
        Trip newTrip = new Trip(tripId, tripName, pois, tripStartDate, tripEndDate);

        if (localTripsId.contains(tripId)) { //if exists a trip with the same id in local
            Trip local = tripHandler.getTrip(tripId); //get this local trip
            if (!local.getPointOfInterestIDSet().equals(pois)) { //if pois are different
                tripHandler.deleteTrip(local);
                tripHandler.saveTrip(newTrip);
            }
        } else {
            tripHandler.saveTrip(newTrip);
        }
    }


    private void mappingDownloadFavourites(Map<String, Object> favFromDB, FavoriteHandler favoriteHandler) {
        Set<String> localFavs = favoriteHandler.getFavoritesIDSet(); //local favourites ids
        for (int i = 0; i < favFromDB.size(); i++) {
            if (!localFavs.contains(favFromDB.get(String.valueOf(i + 1)).toString())) {
                favoriteHandler.addFavorite(favFromDB.get(String.valueOf(i + 1)).toString());
            }
        }
        favoriteHandler.commitChanges();
    }
}
