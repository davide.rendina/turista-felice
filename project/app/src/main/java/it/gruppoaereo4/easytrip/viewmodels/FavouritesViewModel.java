package it.gruppoaereo4.easytrip.viewmodels;

import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import java.util.Set;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;
import it.gruppoaereo4.easytrip.repositories.PointOfInterestRepository;

public class FavouritesViewModel extends ViewModel {

    private MutableLiveData<List<PointOfInterest>> favourites;

    public MutableLiveData<List<PointOfInterest>> getFavourites(Set<String> placeIDs){
        favourites = new MutableLiveData<>();
        //get pois from the repository
        PointOfInterestRepository.getInstance().getPointOfInterests(favourites, placeIDs);
        return favourites;
    }

}
