package it.gruppoaereo4.easytrip.model;


import com.google.gson.annotations.SerializedName;

import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;

public class PointOfInterestAPIResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("result")
    private PointOfInterest result;

    public PointOfInterestAPIResponse(String status, PointOfInterest result) {
        this.status = status;
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public PointOfInterest getPointsOfInterest() {
        return result;
    }

}
