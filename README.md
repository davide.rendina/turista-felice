# Turista felice gruppo GA4
Turista felice è un applicazione per i dispositivi mobili Android che supporta l'esplorazione di una città.
L'applicazione è in grado di:
  1. Suggerire luoghi di interesse per la città fornendo anche informazioni sugli stessi
  2. Notificare all'utente quando c'è nelle vicinanze qualcosa di interessante
  3. Salvare gli elementi di interesse (dividendoli anche per lista)
