package it.gruppoaereo4.easytrip.dbsync;

import android.content.Context;
import android.util.ArraySet;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import it.gruppoaereo4.easytrip.model.Trip;
import it.gruppoaereo4.easytrip.model.persistance.favorites.FavoriteHandler;
import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;

public class UploadWorker extends Worker {

    public UploadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.d("TEST","doWork call");


        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();

        FavoriteHandler favoriteHandler = FavoriteHandler.getInstance();
        TripHandler tripHandler = TripHandler.getInstance();

        //Sync favorites
        syncFavouritesUpdateDB(auth.getUid(), db, favoriteHandler.getFavoritesIDSet());

        //Sync trips
        syncTripsUpdateDB(auth.getUid(), db, tripHandler.getAllTrips());

        return Result.success(); //TODO da sistemare

    }

    //Upload favourites on firebase
    private void syncFavouritesUpdateDB(String uID, FirebaseFirestore db, Set<String> favouritesSet){
            HashMap<String, String> favouritesMap = new HashMap<>();

            for (int i = 0; i < favouritesSet.size(); i++) {//prepare the hashmap for firebase
                favouritesMap.put(String.valueOf(i + 1), favouritesSet.toArray()[i].toString());
            }

            //upload on db
            db.collection("favourites")
                .document(uID)
                .set(favouritesMap).addOnCompleteListener(task -> {
                    //TODO controllo il risultato
                });
    }


    private void syncTripsUpdateDB(String uID, FirebaseFirestore db, Set<Trip> localTrips){

            HashMap<String,Object> mapTrips = new HashMap<>();
            Set<String> localTripsId = new ArraySet<>();

            for(Trip currentTrip : localTrips) { //for each trip

                //parameter used to populate the hashmap
                String tripId = currentTrip.getID();
                String tripName = currentTrip.getName();

                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyy");
                String tripStartDate = dateFormatter.format(new Date(currentTrip.getStartDate().getTime()));
                String tripEndDate = dateFormatter.format(new Date(currentTrip.getEndDate().getTime()));

                mapTrips.put("tripName", tripName);
                mapTrips.put("tripStartDate", tripStartDate);
                mapTrips.put("tripsEndDate", tripEndDate);
                mapTrips.put("poiIDs", Arrays.asList(currentTrip.getPointOfInterestIDSet().toArray()));


                localTripsId.add(tripId);

                //upload on db
                db.collection("trips")
                    .document(uID)
                    .collection("userTrips")
                    .document(tripId)
                    .set(mapTrips)
                    .addOnCompleteListener(task -> {
                        //TODO
                    });
            }

            //call to delete non existent trips on db
            db.collection("trips")
                .document(uID).
                collection("userTrips").
                get().
                addOnCompleteListener(task -> {
                    if(task.isSuccessful() && task.getResult() != null) {
                        for (QueryDocumentSnapshot doc : task.getResult()) {
                            if(!localTripsId.contains(doc.getId())){
                                db.collection("trips")
                                        .document(uID)
                                        .collection("userTrips")
                                        .document(doc.getId())
                                        .delete().addOnCompleteListener(task1 -> {});
                            }
                        }
                    }
                });
    }
}
