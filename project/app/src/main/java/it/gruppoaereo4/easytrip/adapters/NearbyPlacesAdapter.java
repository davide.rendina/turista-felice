package it.gruppoaereo4.easytrip.adapters;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import it.gruppoaereo4.easytrip.MapFragment;
import it.gruppoaereo4.easytrip.R;
import it.gruppoaereo4.easytrip.model.NearbyPlace;
import it.gruppoaereo4.easytrip.model.pointofinterest.PhotoImageReference;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterestTypeMap;

public class NearbyPlacesAdapter extends RecyclerView.Adapter<NearbyPlacesAdapter.NearbyPlacesViewHolder> {

    private List<NearbyPlace> nearbyPlaces;
    private LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Context context;

    public interface OnItemClickListener {
        void onItemClick(NearbyPlace nearbyPlace);
    }

    public NearbyPlacesAdapter(Context context, List<NearbyPlace> nearbyPlaces, OnItemClickListener onItemClickListener) {
        this.layoutInflater = LayoutInflater.from(context);
        this.nearbyPlaces = nearbyPlaces;
        this.onItemClickListener = onItemClickListener;
        this.context = context;
    }

    @Override
    public NearbyPlacesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.card_poi_small, parent, false);
        return new NearbyPlacesViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(NearbyPlacesViewHolder holder, int position) {
        holder.bind(nearbyPlaces.get(position), this.onItemClickListener);
    }

    public int getItemCount() {
        if (nearbyPlaces != null) {
            return nearbyPlaces.size();
        }
        return 0;
    }

    static class NearbyPlacesViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewType;
        ImageView imageViewPhoto, imageViewDirections;
        Context context;

        NearbyPlacesViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            textViewTitle = view.findViewById(R.id.titleFavouriteTextView);
            textViewType = view.findViewById(R.id.typeSmallPOITextView);
            imageViewPhoto = view.findViewById(R.id.photoSmallPOIImageView);
            imageViewDirections = view.findViewById(R.id.directionsSmallPOIImageView);
        }

        void bind(NearbyPlace nearbyPlace, OnItemClickListener onItemClickListener) {
            textViewTitle.setText(nearbyPlace.getName());

            int typeId = nearbyPlace.getTypes() == null ?
                    R.string.pointofinterest_type_general :
                    PointOfInterestTypeMap.getInstance().getStringResourceFromTypesArray(nearbyPlace.getTypes());
            textViewType.setText(context.getString(typeId));

            NearbyPlace.Photo[] photos = nearbyPlace.getPhotos();
            if(photos != null && photos.length > 0) {
                String imageRequestUrl = PhotoImageReference.generateImageRequestURL(
                        800,600, photos[0].getPhoto_reference());

                Picasso.get().load(imageRequestUrl).noFade().into(imageViewPhoto, new Callback() {
                    @Override
                    public void onSuccess() {
                        imageViewPhoto.invalidate();
                    }
                    @Override
                    public void onError(Exception e) {
                        imageViewPhoto.setImageResource(R.drawable.image_not_found);
                    }
                });
            } else {
                imageViewPhoto.setImageResource(R.drawable.image_not_found);
                imageViewPhoto.invalidate();
            }

            imageViewDirections.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Mostro poi sulla mappa
                    Bundle bundle = new Bundle();
                    bundle.putString(MapFragment.MAP_REQUEST.TYPE_KEY, MapFragment.MAP_REQUEST.TYPE_VALUES.SHOW_POINT_OF_INTEREST.toString());
                    bundle.putString(MapFragment.MAP_REQUEST.POI_ID_KEY, nearbyPlace.getId());
                    LatLng latLng = new LatLng(nearbyPlace.getGeometry().getLocation().getLat(), nearbyPlace.getGeometry().getLocation().getLng());
                    bundle.putParcelable(MapFragment.MAP_REQUEST.POI_LATLNG_KEY, latLng);
                    Navigation.findNavController(v).navigate(R.id.action_homeFragment_to_mapFragment, bundle);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(nearbyPlace);
                }
            });
        }
    }
}
