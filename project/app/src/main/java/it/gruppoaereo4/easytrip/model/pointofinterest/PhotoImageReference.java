package it.gruppoaereo4.easytrip.model.pointofinterest;

import com.google.gson.annotations.SerializedName;

import it.gruppoaereo4.easytrip.utils.ApiConstants;

// Models an image reference for a pointOfInterest with Places API
public class PhotoImageReference {

    @SerializedName("height")
    private int height;

    @SerializedName("width")
    private int width;

    @SerializedName("photo_reference")
    private String photoReference;

    /*@SerializedName("html_attributions")
    private List<String> htmlAttribuitions;*/

    public String getPhotoReference() {
        return photoReference;
    }

    private static final String imageRequestUrlPlaceholder = ApiConstants.PLACES_API_BASE_URL +
            "photo?maxwidth=%d&maxheight=%d&photoreference=%s&key=%s";

    public static String generateImageRequestURL(int width, int height, String photoReference){
        return String.format(imageRequestUrlPlaceholder, width, height, photoReference,
                ApiConstants.Keys.GOOGLE_MAPS_API_KEY);
    }
}
