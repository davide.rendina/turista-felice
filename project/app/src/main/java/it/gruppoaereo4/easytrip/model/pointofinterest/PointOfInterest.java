package it.gruppoaereo4.easytrip.model.pointofinterest;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import it.gruppoaereo4.easytrip.R;


public class PointOfInterest implements Parcelable {

    private String placeID;

    @SerializedName("name")
    private String name;

    @SerializedName("formatted_address")
    private String formattedAddress;

    @SerializedName("types")
    private String[] types;

    @SerializedName("business_status")
    private String businessStatus;

    @SerializedName("opening_hours")
    private PointOfInterestOpeningHours openingHours;

    @SerializedName("international_phone_number")
    private String phoneNumber;

    @SerializedName("photos")
    private List<PhotoImageReference> photos;

    private String description;

    private String imageUrl;

    private PointOfInterestGeometry geometry;

    public enum BusinnessStatus{
        OPERATIONAL,
        CLOSED_TEMPORARILY,
        CLOSED_PERMANENTLY,
        NOT_SET
    }


    protected PointOfInterest(Parcel in) {
        placeID = in.readString();
        name = in.readString();
        formattedAddress = in.readString();
        types = in.createStringArray();
        businessStatus = in.readString();
        openingHours = in.readParcelable(PointOfInterestOpeningHours.class.getClassLoader());
        phoneNumber = in.readString();
        description = in.readString();
        imageUrl = in.readString();
        geometry = (PointOfInterestGeometry) in.readValue(PointOfInterestGeometry.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(placeID);
        dest.writeString(name);
        dest.writeString(formattedAddress);
        dest.writeStringArray(types);
        dest.writeString(businessStatus);
        dest.writeParcelable(openingHours, flags);
        dest.writeString(phoneNumber);
        dest.writeString(description);
        dest.writeString(imageUrl);
        dest.writeValue(geometry);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PointOfInterest> CREATOR = new Creator<PointOfInterest>() {
        @Override
        public PointOfInterest createFromParcel(Parcel in) {
            return new PointOfInterest(in);
        }

        @Override
        public PointOfInterest[] newArray(int size) {
            return new PointOfInterest[size];
        }
    };


    public List<PhotoImageReference> getPhotos() {
        return photos;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public String getTodayTime(){
        return openingHours == null ? null : openingHours.getToday();
    }

    public PointOfInterestGeometry getGeometry() {
        return geometry;
    }

    public int getTypeResourceID(){
        return (types == null || types.length == 0) ?
                R.string.pointofinterest_type_general :
                PointOfInterestTypeMap.getInstance().getStringResourceFromTypesArray(types);
    }

    public BusinnessStatus getBusinessStatus() {
        try{
            BusinnessStatus status = BusinnessStatus.valueOf(businessStatus);
            return status;
        } catch (Exception ex){
            return BusinnessStatus.NOT_SET;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description){ this.description = description; }

    public String[][] getTimeTable() {
        if(openingHours == null)
            return null;
        return openingHours.getTimeTable();
    }

    // Return the fields used to retrieve informations from Places API
    public static String getFieldsApiCall(){
        String[] fields = new String[]{"name", "business_status", "formatted_address", "types",
                "photo", "place_id", "name", "opening_hours", "international_phone_number", "geometry"};
        return TextUtils.join(",", fields);
    }
}
