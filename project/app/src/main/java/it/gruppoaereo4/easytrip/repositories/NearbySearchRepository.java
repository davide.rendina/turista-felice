package it.gruppoaereo4.easytrip.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import it.gruppoaereo4.easytrip.model.NearbyPlace;
import it.gruppoaereo4.easytrip.model.NearbySearchAPIResponse;
import it.gruppoaereo4.easytrip.model.Resource;
import it.gruppoaereo4.easytrip.services.NearbySearchService;
import it.gruppoaereo4.easytrip.utils.ApiConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NearbySearchRepository {

    private static final String TAG = "NearbySearchRepository";

    private static NearbySearchRepository instance;
    private NearbySearchService nearbySearchService;

    private final int DEFAULT_RADIUS = 10000;

    private NearbySearchRepository(){
        nearbySearchService = new Retrofit.Builder()
                .baseUrl(ApiConstants.PLACES_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(NearbySearchService.class);
    }

    public static synchronized NearbySearchRepository getInstance(){
        if(instance == null){
            instance = new NearbySearchRepository();
        }
        return instance;
    }

    public void getNearbyPlaces(MutableLiveData<Resource<List<NearbyPlace>>> nearbyPlacesResource, double lat, double lon, String keyword){
        getNearbyPlaces(nearbyPlacesResource, "", lat, lon, DEFAULT_RADIUS, keyword);
    }

    public void getNearbyPlaces(MutableLiveData<Resource<List<NearbyPlace>>> nearbyPlacesResource,
                                String typeFilter, double lat, double lon, int radius, String keyword) {
        String languageAPI = Locale.getDefault().getLanguage().equals("it") ? "it" : "en";
        Call<NearbySearchAPIResponse> call = nearbySearchService.getNearbyPlaces(
                ApiConstants.Keys.GOOGLE_MAPS_API_KEY,
                lat + "," + lon,
                radius,
                languageAPI,
                typeFilter,
                keyword);

        call.enqueue(new Callback<NearbySearchAPIResponse>() {
            @Override
            public void onResponse(@NotNull Call<NearbySearchAPIResponse> call, @NotNull Response<NearbySearchAPIResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Resource<List<NearbyPlace>> resource = new Resource<>();
                    resource.setData(response.body().getNearbyPlaces());
                    resource.setTotalResults(response.body().getNearbyPlaces().size());
                    resource.setStatusCode(response.code());
                    resource.setStatusMessage(response.message());
                    nearbyPlacesResource.postValue(resource);
                } else if (response.errorBody() != null){
                    Resource<List<NearbyPlace>> resource = new Resource<>();
                    resource.setStatusCode(response.code());
                    try {
                        resource.setStatusMessage(response.errorBody().string() + "- " + response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    nearbyPlacesResource.postValue(resource);
                }
            }

            @Override
            public void onFailure(Call<NearbySearchAPIResponse> call, Throwable t) {
                Log.d(TAG, "ERRORE");
            }
        });
    }

}
