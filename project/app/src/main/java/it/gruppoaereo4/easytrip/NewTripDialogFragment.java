package it.gruppoaereo4.easytrip;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import it.gruppoaereo4.easytrip.databinding.FragmentNewTripDialogBinding;
import it.gruppoaereo4.easytrip.model.Trip;
import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;
import it.gruppoaereo4.easytrip.utils.MiscCostants;

public class NewTripDialogFragment extends DialogFragment {

    public static String TAG = "FullScreenDialog";

    private FragmentNewTripDialogBinding binding;
    private Calendar myCalendar;
    private DialogInterface.OnDismissListener onDismissListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentNewTripDialogBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        binding.toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        binding.toolbar.setTitle(R.string.new_trip_toolbar_title);

        //Al click delle due EditText mostro il calendario per selezionare la data
        myCalendar = Calendar.getInstance();
        EditText[] datesET = new EditText[]{binding.dataInizioEditText, binding.dataFineEditText};
        for(EditText dateET:datesET) {
            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel(dateET);
                }
            };
            dateET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(getContext(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
        }

        //Fix stile bottone
        binding.creaViaggioButton.setBackgroundResource(R.drawable.button_rounded);
        binding.creaViaggioButton.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.colorAccent)));

        binding.creaViaggioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nome = binding.nomeViaggioEditText.getText().toString();
                String dataInizio = binding.dataInizioEditText.getText().toString();
                String dataFine = binding.dataFineEditText.getText().toString();
                if(!nome.equals("") && !dataInizio.equals("") && !dataFine.equals("")) {
                    Trip trip = null;
                    try {
                        Date dataPartenza = new SimpleDateFormat(MiscCostants.DATE_FORMAT).parse(dataInizio);
                        Date dataArrivo = new SimpleDateFormat(MiscCostants.DATE_FORMAT).parse(dataFine);
                        if(dataArrivo.after(dataPartenza) || dataArrivo.equals(dataPartenza)) {
                            trip = new Trip(nome, dataPartenza, dataArrivo);
                            TripHandler tripHandler = TripHandler.getInstance();
                            tripHandler.saveTrip(trip);
                            dismiss();
                        } else {
                            Snackbar.make(v, R.string.new_trip_errore_date_scambiate, Snackbar.LENGTH_SHORT).show();
                        }
                    } catch (ParseException e) {
                        Snackbar.make(v, R.string.new_trip_errore_formato_date, Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(v, R.string.new_trip_errore_campi_vuoti, Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateLabel(EditText editText) {
        SimpleDateFormat sdf = new SimpleDateFormat(MiscCostants.DATE_FORMAT, Locale.US);
        editText.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

}
