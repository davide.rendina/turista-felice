package it.gruppoaereo4.easytrip.services;

import it.gruppoaereo4.easytrip.model.NearbySearchAPIResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NearbySearchService {
    @GET("nearbysearch/json")
    Call<NearbySearchAPIResponse> getNearbyPlaces(@Query("key") String key,
                                                    @Query("location") String location,
                                                    @Query("radius") int radius,
                                                    @Query("language") String language,
                                                    @Query("type") String type,
                                                    @Query("keyword") String keyword);
}
