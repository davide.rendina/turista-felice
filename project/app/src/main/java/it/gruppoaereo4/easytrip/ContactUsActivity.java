package it.gruppoaereo4.easytrip;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import it.gruppoaereo4.easytrip.databinding.ActivityContactUsBinding;

public class ContactUsActivity extends AppCompatActivity {

    private ActivityContactUsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityContactUsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(button -> onBackPressed());

        //remove message text
        binding.contactUsActivityCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.contactUsActivityMessageEditText.setText("");
            }
        });


        binding.contactUsActivitySendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkInputs()){ //empty values
                    Snackbar.make(view,R.string.snackbar_compile_all_fields,Snackbar.LENGTH_LONG).show();
                } else {
                    //open email app intent
                    String email = binding.contactUsActivityEmailEditText.getText().toString();
                    String messageTxt = binding.contactUsActivityMessageEditText.getText().toString();
                    Intent intentEmail = new Intent(Intent.ACTION_SEND);
                    intentEmail.setType("message/rfc822");
                    intentEmail.putExtra(Intent.EXTRA_EMAIL  , new String[]{"gruppoaereo4@gmail.com"});
                    intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Contatto da "+email);
                    intentEmail.putExtra(Intent.EXTRA_TEXT, messageTxt);
                    try {
                        startActivity(Intent.createChooser(intentEmail, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Snackbar.make(view,R.string.contact_us_activity_send_fail,Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    private boolean checkInputs(){
        return binding.contactUsActivityEmailEditText.getText().toString().matches("") ||
                binding.contactUsActivityMessageEditText.getText().toString().matches("");
    }
}
