package it.gruppoaereo4.easytrip.services;

import it.gruppoaereo4.easytrip.model.PointOfInterestAPIResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PointOfInterestService {

    @GET("details/json")
    Call<PointOfInterestAPIResponse> getPlaceByID(@Query("place_id") String placeid,
                                                  @Query("fields") String fields,
                                                  @Query("key") String key,
                                                  @Query("language") String language);


}
