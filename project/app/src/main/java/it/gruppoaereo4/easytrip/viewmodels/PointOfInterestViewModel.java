package it.gruppoaereo4.easytrip.viewmodels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.Set;

import it.gruppoaereo4.easytrip.model.Resource;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;
import it.gruppoaereo4.easytrip.repositories.PointOfInterestRepository;

public class PointOfInterestViewModel extends ViewModel {

    private final String TAG = "PoiViewModel";

    private MutableLiveData<PointOfInterest> pointOfInterest;
    private MutableLiveData<List<PointOfInterest>> pointsOfInterest;

    public LiveData<PointOfInterest> getPointOfInterest(String placeID){
        Log.d(TAG,"getPointOfInterest: call");
        if(pointOfInterest == null || ! placeID.equals(pointOfInterest.getValue().getPlaceID())){
            Log.d(TAG,"getPointOfInterest: is pointOfInterest liveData null? "+ (pointOfInterest==null));
            Log.d(TAG,"getPointOfInterest: is the same pointOfInterest ID? "+
                    (pointOfInterest == null? "poi null" : (placeID.equals(pointOfInterest.getValue().getPlaceID()))));
            Log.d(TAG,"getPointOfInterest: call PointOfInterestRepository.getPointOfInterest");

            pointOfInterest = new MutableLiveData<>();
            PointOfInterestRepository.getInstance().getPointOfInterest(pointOfInterest, placeID);
        } /*else {
            pointOfInterest.notify();
        }*/
        return this.pointOfInterest;
    }

    public LiveData<List<PointOfInterest>> getPointsOfInterest (Set<String> placesID) {
        if (pointsOfInterest == null) {
            pointsOfInterest = new MutableLiveData<>();
            PointOfInterestRepository.getInstance().getPointOfInterests(pointsOfInterest, placesID);
        }
        return pointsOfInterest;
    }
}
