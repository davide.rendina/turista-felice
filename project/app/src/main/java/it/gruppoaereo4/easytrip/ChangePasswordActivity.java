package it.gruppoaereo4.easytrip;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import it.gruppoaereo4.easytrip.databinding.ActivityChangePasswordBinding;

public class ChangePasswordActivity extends AppCompatActivity {

    private final String TAG = "ChangePswActivity";

    private ActivityChangePasswordBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChangePasswordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        //get current user
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        binding.changePasswordActivityChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkInputs()){ //empty inputs
                    Snackbar.make(view,R.string.snackbar_compile_all_fields,Snackbar.LENGTH_LONG).show();
                } else{
                    String newPsw = binding.changePasswordActivityNewPswEditText.getText().toString();
                    String oldPsw = binding.changePasswordActivityOldPswEditText.getText().toString();

                    if(!oldPsw.equals(newPsw)){ //passwords are different
                        //get credential and retest a login to see if psw is correct
                        AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), oldPsw);
                        user.reauthenticate(credential)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //update password
                                            user.updatePassword(newPsw).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        Snackbar.make(view,R.string.change_password_activity_snackbar_password_set,Snackbar.LENGTH_LONG ).show();
                                                        Log.d(TAG, "Password reimpostata");
                                                    } else {
                                                        Snackbar.make(view,R.string.change_password_activity_snackbar_cannot_set_password,Snackbar.LENGTH_LONG ).show();
                                                        Log.d(TAG, "Impossibile impostare la password");
                                                    }
                                                }
                                            });
                                        } else {
                                            Snackbar.make(view,R.string.change_password_activity_snackbar_cannot_set_password,Snackbar.LENGTH_LONG ).show();
                                            Log.d(TAG, "Impossibile reimpostare la password");
                                        }
                                    }
                                });
                    } else {
                        Snackbar.make(view,R.string.change_password_activity_snackbar_equals_old_password,Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private boolean checkInputs(){
        return binding.changePasswordActivityOldPswEditText.getText().toString().matches("") ||
                binding.changePasswordActivityNewPswEditText.getText().toString().matches("");
    }
}
