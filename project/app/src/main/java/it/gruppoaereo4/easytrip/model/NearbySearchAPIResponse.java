package it.gruppoaereo4.easytrip.model;



import java.util.List;

public class NearbySearchAPIResponse {

    private String status;
    private List<NearbyPlace> results;

    public NearbySearchAPIResponse(String status, List<NearbyPlace> results) {
        this.status = status;
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public List<NearbyPlace> getNearbyPlaces() {
        return results;
    }
}
