package it.gruppoaereo4.easytrip.dbsync;

import android.content.Context;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

public class SyncHandler {
    public static void uploadData(Context context){
        WorkManager.getInstance(context).enqueue(generateRequest(UploadWorker.class));
    }

    public static void downloadData(Context context){
        WorkManager.getInstance(context).enqueue(generateRequest(DownloadWorker.class));
    }

    public static void uploadAndDownload(Context context){
        WorkManager.getInstance(context)
                .beginWith(generateRequest(UploadWorker.class))
                .then(generateRequest(DownloadWorker.class))
                .enqueue();
    }

    private static OneTimeWorkRequest generateRequest(Class c){
        return new OneTimeWorkRequest.Builder(c)
                .setConstraints(new Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build())
                .build();
    }
}
