package it.gruppoaereo4.easytrip;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;
import androidx.palette.graphics.Palette;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import it.gruppoaereo4.easytrip.databinding.ActivityPointofinterestBinding;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;
import it.gruppoaereo4.easytrip.model.persistance.favorites.FavoriteHandler;
import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;
import it.gruppoaereo4.easytrip.utils.MiscCostants;
import it.gruppoaereo4.easytrip.viewmodels.DescriptionViewModel;

import java.util.Objects;

public class PointOfInterestActivity extends AppCompatActivity implements SelectListDialogFragment.SelectListListener{

    public static final String TAG = "PointOfInterestActivity";
    private ActivityPointofinterestBinding binding;

    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private PointOfInterest representedPOI;

    private FavoriteHandler favoriteHandler;

    private DescriptionViewModel descriptionViewModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Initialize
        super.onCreate(savedInstanceState);
        binding = ActivityPointofinterestBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        representedPOI = getIntent().getParcelableExtra(MiscCostants.INTENT_POI_KEY);
        favoriteHandler = FavoriteHandler.getInstance();
        descriptionViewModel = new DescriptionViewModel();

        // Call API description
        callDescriptionAPI();
        Log.d(TAG, representedPOI.toString());

        // Set listener for show/hide timetable
        binding.timetableTextCardView.setOnClickListener(v -> {
            //Show or dismiss timetable
            binding.timetableTableLayout.setVisibility(
                    binding.timetableTableLayout.getVisibility()==View.VISIBLE ? View.GONE : View.VISIBLE
            );
            // Rotate the image
            ImageView arrowImageView = binding.timetableArrowImageButton;
            arrowImageView.animate().rotation(Math.abs(arrowImageView.getRotation()-180)).start();
        });
        findViewById(R.id.timetable_tableLayout).setVisibility(View.GONE);

        // Set listener for telephone
        binding.telephoneTextView.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", representedPOI.getPhoneNumber(), null));
            startActivity(intent);
        });

        // Initialize toolbar
        initToolbar();

        // Set listeners for favorite and add to list buttons
        binding.addListImageButton.setOnClickListener(view -> {
            new SelectListDialogFragment().show(getSupportFragmentManager(), "SelectList");
        });

        binding.favoriteImageButton.setOnClickListener(v -> {
            changeFavoriteImageButtonColor(favoriteHandler.toggleFavorite(representedPOI.getPlaceID()));
        });

        // Fill UI with point of interest data
        setPOIInfoUI();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        favoriteHandler.commitChanges();
    }

    private void callDescriptionAPI() {
        descriptionViewModel.getPlace(this.representedPOI.getName()).observe(this, description -> {
            Log.d(TAG, "Added description: "+description);
            representedPOI.setDescription(description);
            binding.descriptionTextView.setText(description);
        });
    }

    private void changeFavoriteImageButtonColor(boolean isFavorite) {
        binding.favoriteImageButton.getDrawable().setTint(
                isFavorite ? Color.RED : getColor(R.color.colorPrimary)
        );
    }

    private void initToolbar(){
        //Retrieve Views
        toolbar = findViewById(R.id.toolbar);
        collapsingToolbarLayout = findViewById(R.id.appbar_collapsing_layout);
        AppBarLayout appbar = findViewById(R.id.appbar_layout);

        //Set action bar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout.setTitleEnabled(false);

        //Set new listener
        appbar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            if ((collapsingToolbarLayout.getHeight() + verticalOffset) < (2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout))) {
                // Collapsed
                toolbar.setTitle(representedPOI.getName());
                toolbar.setTitleTextColor(Color.WHITE);
                Objects.requireNonNull(toolbar.getNavigationIcon()).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            } else {
                // Not collapsed
                Objects.requireNonNull(toolbar.getNavigationIcon()).setColorFilter(getBackIconDynamicColor(), PorterDuff.Mode.SRC_ATOP);
                toolbar.setTitle("");
            }
        });

        //Set back button
        toolbar.setNavigationOnClickListener(button -> onBackPressed());
    }

    // Get the correct color for toolbar back arrow when it's transparent in order to the image displayed
    private int getBackIconDynamicColor(){
        // Get the size of the portion of image displayed below the icon
        int cropLength = toolbar.getNavigationIcon().getIntrinsicHeight();
        int cropStart = toolbar.getNavigationIcon().getBounds().centerX();

        // Get the BitmapDrawable from the imageView
        BitmapDrawable bitmapDrawable = (BitmapDrawable) binding.appBarImage.getDrawable();
        if(bitmapDrawable != null) {
            // Crop the Bitmap according to back arrow size
            Bitmap croppedImage = Bitmap.createBitmap(bitmapDrawable.getBitmap(), cropStart, cropStart, cropLength, cropLength);

            // Generate the Palette in order to get the color has the best contrast with the image
            Palette palette = Palette.from(croppedImage).generate();
            Palette.Swatch dominantSwatch = palette.getDominantSwatch();
            return dominantSwatch == null ? Color.WHITE : dominantSwatch.getBodyTextColor();
        }
        else
            return Color.WHITE;
    }

    private void setPOIInfoUI(){
        binding.titleTextView.setText(representedPOI.getName());
        binding.addressTextView.setText(representedPOI.getFormattedAddress());

        // Update UI with telephone number
        String telephoneNumber = representedPOI.getPhoneNumber();
        if(telephoneNumber == null){
            telephoneNumber = getString(R.string.poi_activity_telephonenotfound);
            binding.telephoneTextView.setClickable(false);
        }
        binding.telephoneTextView.setText(telephoneNumber);

        // Update UI with time informations
        setTimeInfoUI();

        // Update UI with type
        binding.typeTextView.setText(representedPOI.getTypeResourceID());

        // Load picture
        try {
            Picasso.get().load(representedPOI.getImageUrl()).into(binding.appBarImage);
        }
        catch (Exception ex){
            binding.appBarImage.setImageResource(R.drawable.image_not_found);
        }
        binding.appBarImage.invalidate();

        // If is a favorite poi sets the red heart
        changeFavoriteImageButtonColor(favoriteHandler.isFavorite(representedPOI.getPlaceID()));
    }

    private void setTimeInfoUI() {
        // Update timetable or disable it if the current point of interest is Closed or if time not available
        switch (representedPOI.getBusinessStatus()){
            case OPERATIONAL:
                String todayText = representedPOI.getTodayTime();
                if(todayText == null){
                    binding.timetableTextView.setText(R.string.poi_time_not_available);
                    binding.timetableTextCardView.setClickable(false);
                }
                else {
                    String[][] timeTable = representedPOI.getTimeTable();
                    //Sets today time text view
                    binding.timetableTextView.setText(todayText);

                    //Sets days of week table
                    binding.mondayTextView.setText(timeTable[0][0]);
                    binding.mondayTimeTextView.setText(timeTable[0][1]);
                    binding.tuesdayTextView.setText(timeTable[1][0]);
                    binding.tuesdayTimeTextView.setText(timeTable[1][1]);
                    binding.wednesdayTextView.setText(timeTable[2][0]);
                    binding.wednesdayTimeTextView.setText(timeTable[2][1]);
                    binding.thursdayTextView.setText(timeTable[3][0]);
                    binding.thursdayTimeTextView.setText(timeTable[3][1]);
                    binding.fridayTextView.setText(timeTable[4][0]);
                    binding.fridayTimeTextView.setText(timeTable[4][1]);
                    binding.saturdayTextView.setText(timeTable[5][0]);
                    binding.saturdayTimeTextView.setText(timeTable[5][1]);
                    binding.sundayTextView.setText(timeTable[6][0]);
                    binding.sundayTimeTextView.setText(timeTable[6][1]);

                    binding.timetableTextCardView.setClickable(true);
                }
                break;
            case CLOSED_TEMPORARILY:
                binding.timetableTextView.setText(R.string.poi_time_closed_temporarily);
                binding.timetableTextCardView.setClickable(false);
                break;
            case CLOSED_PERMANENTLY:
                binding.timetableTextView.setText(R.string.poi_time_closed_permanently);
                binding.timetableTextCardView.setClickable(false);
                break;
            case NOT_SET:
                binding.timetableTextView.setText(R.string.poi_time_not_available);
                binding.timetableTextCardView.setClickable(false);
                break;
            default:
                break;
        }

        // If timetable is not available, hides the arrow
        binding.timetableArrowImageButton.setVisibility(
                binding.timetableTextCardView.isClickable() ? View.VISIBLE : View.INVISIBLE
        );
    }

    @Override
    public void onDialogItemClick(DialogFragment dialogFragment, String listId, String listName) {
        // Receive itemClick from the DialogFragment for handling lists
        Snackbar.make(binding.coordinatorLayout,
                representedPOI.getName()+" aggiunto alla lista '"+listName + "'",
                Snackbar.LENGTH_SHORT).show();

        // Add current pointOfInterest to the selected list
        TripHandler.getInstance().addPointOfInterestToTrip(listId, representedPOI.getPlaceID());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(0, intent);
        finish();
    }
}
