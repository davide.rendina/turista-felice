package it.gruppoaereo4.easytrip;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import it.gruppoaereo4.easytrip.adapters.TripsAdapter;
import it.gruppoaereo4.easytrip.model.Trip;
import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;
import java.util.Set;

import it.gruppoaereo4.easytrip.databinding.FragmentTripsBinding;

public class TripsFragment extends Fragment {

    private static final String TAG = "TripsFragment";

    private FragmentTripsBinding binding;

    public static TripsFragment newInstance() {
        return new TripsFragment();
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentTripsBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ((AppCompatActivity)getActivity()).setSupportActionBar(binding.toolbar2);
        binding.toolbar2.setTitle(R.string.trips_fragment_toolbar_title);

        TripHandler tripHandler = TripHandler.getInstance();

        Set<Trip> trips = tripHandler.getAllTrips();

        //Se ci sono viaggi li mostro nella recyclerview con l'adapter, altrimenti mostro un messaggio
        if(trips.size() > 0) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            binding.tripsRecyclerView.setLayoutManager(layoutManager);

            TripsAdapter tripsAdapter = new TripsAdapter(getActivity(), getViewLifecycleOwner(), trips, new TripsAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Trip trip) {
                    Intent intent = new Intent(getContext(), TripActivity.class);
                    intent.putExtra(TripActivity.TRIP_EXTRA_KEY, trip);
                    startActivityForResult(intent, 1);
                }
            });
            binding.tripsRecyclerView.setAdapter(tripsAdapter);
        } else {
            binding.noTripsTextView.setVisibility(View.VISIBLE);
        }

        Fragment fragment = this;
        binding.newTripFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewTripDialogFragment dialog = new NewTripDialogFragment();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        //Alla chiusura del new trip dialog aggiorno il fragment
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.detach(fragment).attach(fragment).commit();
                    }
                });
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, NewTripDialogFragment.TAG);

            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Quando arriva il result 0 aggiorno il fragment
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == 0) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(this).attach(this).commit();
            }
        }
    }

}