package it.gruppoaereo4.easytrip.model;

import java.util.Arrays;

public class NearbyPlace {

    private String place_id;
    private String name;
    private String[] types;
    private Geometry geometry;
    private Photo[] photos;

    public NearbyPlace(String id, String name, String[] types, Geometry geometry, Photo[] photos) {
        this.place_id = id;
        this.name = name;
        this.types = types;
        this.geometry = geometry;
        this.photos = photos;
    }

    public String getId() {
        return place_id;
    }

    public void setId(String id) {
        this.place_id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Photo[] getPhotos() {
        return photos;
    }

    public void setPhotos(Photo[] photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {
        return "NearbyPlace{" +
                "name='" + name + '\'' +
                ", types=" + Arrays.toString(types) +
                ", geometry=" + geometry +
                ", photos=" + Arrays.toString(photos) +
                '}';
    }

    public class Photo {
        private String photo_reference;

        public Photo(String photo_reference) {
            this.photo_reference = photo_reference;
        }

        public String getPhoto_reference() {
            return photo_reference;
        }

        public void setPhoto_reference(String photo_reference) {
            this.photo_reference = photo_reference;
        }

        @Override
        public String toString() {
            return "Photo{" +
                    "photo_reference='" + photo_reference + '\'' +
                    '}';
        }
    }

    public class Geometry {
        private Location location;

        public Geometry(Location location) {
            this.location = location;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        @Override
        public String toString() {
            return "Geometry{" +
                    "location=" + location +
                    '}';
        }

        public class Location {
            private double lat;
            private double lng;

            public Location(double lat) {
                this.lat = lat;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            @Override
            public String toString() {
                return "Location{" +
                        "lat=" + lat +
                        ", lng=" + lng +
                        '}';
            }
        }

    }

}