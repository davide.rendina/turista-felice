package it.gruppoaereo4.easytrip.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import it.gruppoaereo4.easytrip.repositories.DescriptionRepository;

public class DescriptionViewModel extends ViewModel {

    private MutableLiveData<String> description;

    public LiveData<String> getPlace(String poiName){
        if(description == null){
            description = new MutableLiveData<>();
            DescriptionRepository.getInstance().getDescription(description, poiName);
        }
        return this.description;
    }

}
