package it.gruppoaereo4.easytrip;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;


public class SelectListDialogFragment extends DialogFragment {

    private static final String TAG = SelectListDialogFragment.class.getName();
    
    public interface SelectListListener{
        // Interface for comunication between DialogFragment and the activity that launched it
        void onDialogItemClick(DialogFragment dialogFragment, String listId, String listName);
    }
    SelectListListener listener;

    private String[] names;
    private String[] ids;

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        // Retrieve all the list defined
        retrieveLists();

        // Generate the Dialog
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getActivity());
        builder.setTitle(getResources().getString(R.string.poidialogfragment_selectlist_title));
        builder.setItems(names, (dialog, which) -> {
            listener.onDialogItemClick(SelectListDialogFragment.this, ids[which], names[which]);
        });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (SelectListListener) context;
        } catch (ClassCastException e) {
            Log.d(TAG, "onAttach: exception "+e.getMessage());
        }
    }

    private void retrieveLists(){
        // Use TripHandler class for retrieving all trip names and ids
        TripHandler handler = TripHandler.getInstance();
        Map<String, String> tripsMap = handler.getTripsMap();
        int arraySize = tripsMap.size();

        // Generate two arrays for handling the list selection
        names= new String[arraySize];
        ids = new String[arraySize];
        int i = 0;
        for(Map.Entry<String, String> entry : tripsMap.entrySet()){
            names[i] = entry.getValue();
            ids[i] = entry.getKey();
            i++;
        }
    }

}
