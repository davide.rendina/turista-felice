package it.gruppoaereo4.easytrip;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import it.gruppoaereo4.easytrip.adapters.TripPlacesAdapter;
import it.gruppoaereo4.easytrip.databinding.ActivityTripBinding;
import it.gruppoaereo4.easytrip.model.Trip;
import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;
import it.gruppoaereo4.easytrip.utils.MiscCostants;
import it.gruppoaereo4.easytrip.viewmodels.PointOfInterestViewModel;

public class TripActivity extends AppCompatActivity {

    private static final String TAG = "TripActivity";

    public static final String TRIP_EXTRA_KEY = "trip";

    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ActivityTripBinding binding;
    private Trip trip;
    private PointOfInterestViewModel pointOfInterestViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTripBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        trip = getIntent().getParcelableExtra(TRIP_EXTRA_KEY);
        initToolbar();

        String imageUrl = trip.getImageUrl();
        if (imageUrl.equals("")) {
            binding.appBarImage.setImageResource(R.drawable.image_not_found);
            binding.appBarImage.invalidate();
        } else {
            Picasso.get().load(imageUrl).noFade().into(binding.appBarImage, new Callback() {
                @Override
                public void onSuccess() {
                    binding.appBarImage.invalidate();
                }

                @Override
                public void onError(Exception e) {
                    binding.appBarImage.setImageResource(R.drawable.image_not_found);
                }
            });
        }

        binding.nameTripTextView.setText(trip.getName());
        binding.dateTripTextView.setText(new SimpleDateFormat(MiscCostants.DATE_FORMAT).format(trip.getStartDate())
                                            + " - " +
                                            new SimpleDateFormat(MiscCostants.DATE_FORMAT).format(trip.getEndDate()));
        Set<String> places = trip.getPointOfInterestIDSet();
        binding.numPlacesTripTextView.setText(Integer.toString(places.size()));

        //Se ci sono places li mostro nella recyclerview con l'adapter, altrimenti mostro un messaggio per dire che non ci sono places
        if(places.size() > 0) {
            pointOfInterestViewModel = new ViewModelProvider(this).get(PointOfInterestViewModel.class);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            binding.placesTripRecyclerView.setLayoutManager(layoutManager);

            Context context = this;
            final Observer<List<PointOfInterest>> observer = new Observer<List<PointOfInterest>>() {
                @Override
                public void onChanged(List<PointOfInterest> pointsOfInterest) {
                    TripPlacesAdapter tripPlacesAdapter = new TripPlacesAdapter(trip, context, pointsOfInterest, new TripPlacesAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(PointOfInterest poi) {
                            Intent intent = new Intent(context, PointOfInterestActivity.class);
                            intent.putExtra(MiscCostants.INTENT_POI_KEY, poi);
                            startActivity(intent);
                        }
                    });
                    binding.placesTripRecyclerView.setAdapter(tripPlacesAdapter);
                }
            };

            LiveData<List<PointOfInterest>> liveData = pointOfInterestViewModel.getPointsOfInterest(places);
            liveData.observe(this, observer);
        } else {
            binding.noTripPlacesTextView.setVisibility(View.VISIBLE);
        }

    }

    @SuppressLint("ResourceType")
    private void initToolbar(){
        //Retrieve Views
        toolbar = findViewById(R.id.toolbar);
        collapsingToolbarLayout = findViewById(R.id.appbar_collapsing_layout);
        AppBarLayout appbar = findViewById(R.id.appbar_layout);

        //Set action bar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout.setTitleEnabled(false);

        //Set new listener
        appbar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            if ((collapsingToolbarLayout.getHeight() + verticalOffset) < (2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout))) {
                // Collapsed
                toolbar.setTitle(trip.getName());
                toolbar.setTitleTextColor(Color.WHITE);
                Objects.requireNonNull(toolbar.getNavigationIcon()).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                try {
                    toolbar.getMenu().getItem(0).getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                } catch (Exception e) {}
            } else {
                // Not collapsed
                Objects.requireNonNull(toolbar.getNavigationIcon()).setColorFilter(getBackIconDynamicColor(), PorterDuff.Mode.SRC_ATOP);
                toolbar.setTitle("");
                try {
                    toolbar.getMenu().getItem(0).getIcon().setColorFilter(getBackIconDynamicColor(), PorterDuff.Mode.SRC_ATOP);
                } catch (Exception e) {}
            }
        });

        //Set back button
        toolbar.setNavigationOnClickListener(button -> onBackPressed());

        Context context = this;
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId() == R.id.action_delete) {
                    new AlertDialog.Builder(context).setMessage(R.string.title_dialog_cancella_viaggio)
                        .setPositiveButton(R.string.positive_dialog_cancella_viaggio, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                TripHandler tripHandler = TripHandler.getInstance();
                                tripHandler.deleteTrip(trip);
                                onBackPressed();
                            }
                        })
                        .setNegativeButton(R.string.negative_dialog_cancella_viaggio, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {}
                        })
                        .show();
                    return true;
                }
                return false;
            }
        });
    }

    // Get the correct color for toolbar back arrow when it's transparent in order to the image displayed
    private int getBackIconDynamicColor(){
        // Get the size of the portion of image displayed below the icon
        int cropLength = toolbar.getNavigationIcon().getIntrinsicHeight();
        int cropStart = toolbar.getNavigationIcon().getBounds().centerX();

        // Get the BitmapDrawable from the imageView
        BitmapDrawable bitmapDrawable = (BitmapDrawable) binding.appBarImage.getDrawable();
        if(bitmapDrawable != null) {
            // Crop the Bitmap according to back arrow size
            Bitmap croppedImage = Bitmap.createBitmap(bitmapDrawable.getBitmap(), cropStart, cropStart, cropLength, cropLength);

            // Generate the Palette in order to get the color has the best contrast with the image
            Palette palette = Palette.from(croppedImage).generate();
            Palette.Swatch dominantSwatch = palette.getDominantSwatch();
            return dominantSwatch == null ? Color.WHITE : dominantSwatch.getBodyTextColor();
        }
        else
            return Color.WHITE;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.trip_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Mando result per aggiornare il fragment
        Intent intent = new Intent();
        setResult(0, intent);
        finish();
    }
}
