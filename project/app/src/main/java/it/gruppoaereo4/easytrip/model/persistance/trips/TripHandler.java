package it.gruppoaereo4.easytrip.model.persistance.trips;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import it.gruppoaereo4.easytrip.model.Trip;
import it.gruppoaereo4.easytrip.utils.SharedPreferencesKeys.TripsKeys;


public class TripHandler {

    // Every trip is saved on the disk in a Shared Pref. file with name: <prefixTripFilename><tripID>
    // Example: trip with ID XXX is saved on the disk in the sharedPref 'tripXXX'
    private final String prefixTripFilename = "trip";
    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
    private Context context;
    private boolean modified = false;
    // Map every trip key with its name, for every trip created.
    private Map<String, String> tripsMap;

    private static TripHandler instance;


    public void init(Context context) {
        this.context = context;
        populateTripsMap();
    }

    private TripHandler(){}

    public static TripHandler getInstance() {
        if(instance == null)
            instance = new TripHandler();
        return instance;
    }


    // Retrieve trip keys and names from disk and populate tripsMap
    private void populateTripsMap(){
        tripsMap = new HashMap<>();
        SharedPreferences sharedPreferences = context.getSharedPreferences(TripsKeys.TRIPLIST_FILENAME, Context.MODE_PRIVATE);
        Set<String> allTripsKeys = sharedPreferences.getAll().keySet();
        for(String key : allTripsKeys){
            tripsMap.put(key, sharedPreferences.getString(key, ""));
        }
    }

    public Map<String, String> getTripsMap() {
        return tripsMap;
    }

    // Add the trip to tripsMap
    private void addTripsMapEntry(Trip trip){
        SharedPreferences.Editor editor = context.getSharedPreferences(TripsKeys.TRIPLIST_FILENAME, Context.MODE_PRIVATE).edit();
        editor.putString(trip.getID(), trip.getName());
        editor.apply();

        tripsMap.put(trip.getID(), trip.getName());
        modified = true;
    }

    // Remove the trip from tripsMap
    private void removeTripsMapEntry(Trip trip){
        SharedPreferences.Editor editor = context.getSharedPreferences(TripsKeys.TRIPLIST_FILENAME, Context.MODE_PRIVATE).edit();
        editor.remove(trip.getID());
        editor.apply();

        tripsMap.remove(trip.getID());
        modified = true;
    }

    // Save the trip to the disk
    public void saveTrip(Trip trip){
        addTripsMapEntry(trip);

        SharedPreferences.Editor editor = context.getSharedPreferences(getTripSharedPrefName(trip), Context.MODE_PRIVATE).edit();
        editor.putString(TripsKeys.TRIP_NAME, trip.getName());
        editor.putString(TripsKeys.TRIP_ID, trip.getID());
        editor.putStringSet(TripsKeys.TRIP_POI_LIST, trip.getPointOfInterestIDSet());
        editor.putString(TripsKeys.TRIP_START_DATE, dateFormatter.format(trip.getStartDate()));
        editor.putString(TripsKeys.TRIP_END_DATE, dateFormatter.format(trip.getEndDate()));
        editor.apply();
    }

    // Get trip from disk
    public Trip getTrip(String tripId){
        Date startDate, endDate;
        SharedPreferences sharedPreferences = context.getSharedPreferences(getTripSharedPrefName(tripId), Context.MODE_PRIVATE);
        String tripName = sharedPreferences.getString(TripsKeys.TRIP_NAME, "");
        Set<String> tripPointOfInterestSet = sharedPreferences.getStringSet(TripsKeys.TRIP_POI_LIST, new HashSet<>());
        
        try {
            startDate = dateFormatter.parse(sharedPreferences.getString(TripsKeys.TRIP_START_DATE, "2000-01-01"));
            endDate = dateFormatter.parse(sharedPreferences.getString(TripsKeys.TRIP_END_DATE, "2000-01-01"));
        } catch (ParseException ex){
            startDate = endDate = new Date();
        }

        return new Trip(tripId, tripName, tripPointOfInterestSet, startDate, endDate);
    }

    // Remove the trip from the disk
    public void deleteTrip(Trip trip){
        removeTripsMapEntry(trip);

        SharedPreferences.Editor editor = context.getSharedPreferences(getTripSharedPrefName(trip), Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
        modified = true;
    }

    // Get all trips from disk
    public Set<Trip> getAllTrips(){
        Set<Trip> trips = new HashSet<>();
        for(String tripID: tripsMap.keySet())
            trips.add(getTrip(tripID));
        return trips;
    }

    // Add the pointOfInterest with poiID to the trip with ID tripID
    public void addPointOfInterestToTrip(String tripID, String poiID){
        SharedPreferences sharedPreferences = context.getSharedPreferences(getTripSharedPrefName(tripID), Context.MODE_PRIVATE);
        Set<String> points = new HashSet<>();
        points.addAll(sharedPreferences.getStringSet(TripsKeys.TRIP_POI_LIST, new HashSet<>()));
        points.add(poiID);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(TripsKeys.TRIP_POI_LIST, points);
        editor.apply();
        modified = true;
    }

    public void removePointOfInterestToTrip(String tripID, String poiID){
        SharedPreferences sharedPreferences = context.getSharedPreferences(getTripSharedPrefName(tripID), Context.MODE_PRIVATE);
        Set<String> points = new HashSet<>();
        points.addAll(sharedPreferences.getStringSet(TripsKeys.TRIP_POI_LIST, new HashSet<>()));
        points.remove(poiID);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(TripsKeys.TRIP_POI_LIST, points);
        editor.apply();
        modified = true;
    }

    public int getNumberOfTrips(){
        return tripsMap == null ? 0 : tripsMap.values().size();
    }

    // Returns the name of the Shared Pref. used to store this Trip
    private String getTripSharedPrefName(Trip trip){
        return getTripSharedPrefName(trip.getID());
    }

    private String getTripSharedPrefName(String tripID){
        return prefixTripFilename+tripID;
    }

    public boolean isModified(){
        return modified;
    }

}
