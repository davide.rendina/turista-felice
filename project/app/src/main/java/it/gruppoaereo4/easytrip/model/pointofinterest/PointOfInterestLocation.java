package it.gruppoaereo4.easytrip.model.pointofinterest;

import android.os.Parcel;
import android.os.Parcelable;

public class PointOfInterestLocation implements Parcelable{

    private double lat;
    private double lng;

    public PointOfInterestLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "PointOfInterestLocation{" +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }

    protected PointOfInterestLocation(Parcel in) {
        lat = in.readDouble();
        lng = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(lat);
        dest.writeDouble(lng);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PointOfInterestLocation> CREATOR = new Parcelable.Creator<PointOfInterestLocation>() {
        @Override
        public PointOfInterestLocation createFromParcel(Parcel in) {
            return new PointOfInterestLocation(in);
        }

        @Override
        public PointOfInterestLocation[] newArray(int size) {
            return new PointOfInterestLocation[size];
        }
    };
}
