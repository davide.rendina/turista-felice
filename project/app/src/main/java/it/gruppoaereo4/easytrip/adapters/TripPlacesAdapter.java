package it.gruppoaereo4.easytrip.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import it.gruppoaereo4.easytrip.MainActivity;
import it.gruppoaereo4.easytrip.MapFragment;
import it.gruppoaereo4.easytrip.R;
import it.gruppoaereo4.easytrip.model.Trip;
import it.gruppoaereo4.easytrip.model.persistance.trips.TripHandler;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;

public class TripPlacesAdapter extends RecyclerView.Adapter<TripPlacesAdapter.TripPlacesViewHolder> {

    private List<PointOfInterest> pointsOfInterest;
    private LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Context context;
    private Trip trip;

    public interface OnItemClickListener {
        void onItemClick(PointOfInterest poi);
    }

    public TripPlacesAdapter(Trip trip, Context context, List<PointOfInterest> pointsOfInterest, OnItemClickListener onItemClickListener) {
        this.trip = trip;
        this.layoutInflater = LayoutInflater.from(context);
        this.pointsOfInterest = pointsOfInterest;
        this.onItemClickListener = onItemClickListener;
        this.context = context;
    }

    @Override
    public TripPlacesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.card_poi_trip, parent, false);
        return new TripPlacesViewHolder(view, context, trip, this, pointsOfInterest);
    }

    @Override
    public void onBindViewHolder(TripPlacesViewHolder holder, int position) {
        holder.bind(pointsOfInterest.get(position), this.onItemClickListener);
    }

    public int getItemCount() {
        if (pointsOfInterest != null) {
            return pointsOfInterest.size();
        }
        return 0;
    }

    static class TripPlacesViewHolder extends RecyclerView.ViewHolder {

        Context context;
        TextView textViewTitle, textViewType, textViewTime;
        ImageView imageViewImage, imageViewRemove, imageViewDirections;
        Trip trip;
        RecyclerView.Adapter adapter;
        List<PointOfInterest> pointsOfInterest;

        TripPlacesViewHolder(View view, Context context, Trip trip, RecyclerView.Adapter adapter, List<PointOfInterest> pointsOfInterest) {
            super(view);
            this.context = context;
            this.trip = trip;
            this.adapter = adapter;
            this.pointsOfInterest = pointsOfInterest;
            textViewTitle = view.findViewById(R.id.title_textView);
            textViewType = view.findViewById(R.id.type_textView);
            textViewTime = view.findViewById(R.id.time_textView);
            imageViewImage = view.findViewById(R.id.imageView_card_pointofinterest);
            imageViewRemove = view.findViewById(R.id.remove_imageView);
            imageViewDirections = view.findViewById(R.id.directionsSmallPOIImageView);
        }

        void bind(PointOfInterest poi, OnItemClickListener onItemClickListener) {
            if(poi != null) {
                textViewTitle.setText(poi.getName());
                textViewType.setText(poi.getTypeResourceID());

                PointOfInterest.BusinnessStatus businnessStatus = poi.getBusinessStatus();
                String timeText;
                switch (businnessStatus) {
                    case OPERATIONAL:
                        timeText = poi.getTodayTime() == null ?
                                context.getString(R.string.poi_time_not_available) :
                                poi.getTodayTime();
                        break;
                    case CLOSED_TEMPORARILY:
                        timeText = context.getString(R.string.poi_time_closed_temporarily);
                        break;
                    case CLOSED_PERMANENTLY:
                        timeText = context.getString(R.string.poi_time_closed_permanently);
                        break;
                    case NOT_SET:
                    default:
                        timeText = context.getString(R.string.poi_time_not_available);
                        break;
                }
                textViewTime.setText(timeText);

                try {
                    String imageUrl = poi.getImageUrl();
                    if (imageUrl == null) {
                        imageViewImage.setImageResource(R.drawable.image_not_found);
                        imageViewImage.invalidate();
                    } else {
                        Picasso.get().load(imageUrl).noFade().into(imageViewImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                imageViewImage.invalidate();
                            }

                            @Override
                            public void onError(Exception e) {
                                imageViewImage.setImageResource(R.drawable.image_not_found);
                            }
                        });
                    }
                } catch (Exception ex) {
                    imageViewImage.setImageResource(R.drawable.image_not_found);
                }

                int position = this.getAdapterPosition();
                imageViewRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Rimuovo place dal viaggio e notifico il livedata
                        TripHandler tripHandler = TripHandler.getInstance();
                        tripHandler.removePointOfInterestToTrip(trip.getID(), poi.getPlaceID());
                        pointsOfInterest.remove(position);
                        adapter.notifyItemRemoved(position);
                        TextView counter = ((TextView) itemView.getRootView().findViewById(R.id.numPlacesTripTextView));
                        counter.setText(Integer.toString(pointsOfInterest.size()));
                    }
                });

                imageViewDirections.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Invio il poi all'activity host per mostrarlo sulla mappa
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra(MainActivity.POI_EXTRA_KEY, poi);
                        context.startActivity(intent);
                    }
                });

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.onItemClick(poi);
                    }
                });
            }
        }
    }

}