package it.gruppoaereo4.easytrip.model.pointofinterest;

import android.os.Parcel;
import android.os.Parcelable;

public class PointOfInterestGeometry implements Parcelable {

    private PointOfInterestLocation location;

    public PointOfInterestGeometry(PointOfInterestLocation location) {
        this.location = location;
    }

    public PointOfInterestLocation getLocation() {
        return location;
    }

    public void setLocation(PointOfInterestLocation location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "PointOfInterestGeometry{" +
                "location=" + location +
                '}';
    }

    protected PointOfInterestGeometry(Parcel in) {
        location = (PointOfInterestLocation) in.readValue(PointOfInterestLocation.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(location);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PointOfInterestGeometry> CREATOR = new Parcelable.Creator<PointOfInterestGeometry>() {
        @Override
        public PointOfInterestGeometry createFromParcel(Parcel in) {
            return new PointOfInterestGeometry(in);
        }

        @Override
        public PointOfInterestGeometry[] newArray(int size) {
            return new PointOfInterestGeometry[size];
        }
    };
}
