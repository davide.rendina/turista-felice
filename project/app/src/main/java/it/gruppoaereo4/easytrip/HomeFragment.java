package it.gruppoaereo4.easytrip;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import java.util.Arrays;
import java.util.List;

import it.gruppoaereo4.easytrip.adapters.NearbyPlacesAdapter;
import it.gruppoaereo4.easytrip.databinding.FragmentHomeBinding;
import it.gruppoaereo4.easytrip.model.NearbyPlace;
import it.gruppoaereo4.easytrip.model.Resource;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;
import it.gruppoaereo4.easytrip.utils.ApiConstants;
import it.gruppoaereo4.easytrip.utils.MiscCostants;
import it.gruppoaereo4.easytrip.viewmodels.NearbySearchViewModel;
import it.gruppoaereo4.easytrip.viewmodels.PointOfInterestViewModel;

public class HomeFragment extends Fragment {
    // Values for request
    private final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private final int ENABLE_GPS_REQUEST_CODE = 2;

    private static final String TAG = "HomeFragment";

    private NearbySearchViewModel nearbySearchViewModel;
    private FragmentHomeBinding binding;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private MutableLiveData<LatLng> location;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    private Observer<LatLng> myLocationObserver = latLng -> {
        if(latLng != null) {
            //Mostro nearbyPlaces solo se la geolocalizzazione è attiva
            getNearbyPlaces(latLng.latitude, latLng.longitude);
            binding.nearbyPlacesRecyclerView.setVisibility(View.VISIBLE);
            binding.nearbyPlacesTextView.setVisibility(View.VISIBLE);
            binding.geolocationImageButton.setVisibility(View.VISIBLE);
            binding.geolocationImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Navigation.findNavController(binding.getRoot()).navigate(R.id.action_homeFragment_to_mapFragment);
                }
            });
        }
    };

    @SuppressLint("MissingPermission")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        initializeAutocompleteSearchFragment();

        binding.nearbyPlacesRecyclerView.setVisibility(View.GONE);
        binding.nearbyPlacesTextView.setVisibility(View.GONE);
        binding.geolocationImageButton.setVisibility(View.INVISIBLE);

        binding.profileImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), UserProfileActivity.class);
                startActivity(intent);
            }
        });

        View.OnClickListener categoriesOnClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                switch(view.getId()) {
                    case R.id.restaurantImageButton:
                    case R.id.restaurantTextView:
                        bundle.putString(MapFragment.MAP_REQUEST.TYPE_KEY, MapFragment.MAP_REQUEST.TYPE_VALUES.NEARBY_SEARCH.toString());
                        bundle.putString(MapFragment.MAP_REQUEST.CATEGORY_NEARBY_KEY, MapFragment.MAP_REQUEST.CATEGORY_NEARBY_VALUES.RESTAURANT.toString());
                        break;
                    case R.id.hotelImageButton:
                    case R.id.hotelTextView:
                        bundle.putString(MapFragment.MAP_REQUEST.TYPE_KEY, MapFragment.MAP_REQUEST.TYPE_VALUES.NEARBY_SEARCH.toString());
                        bundle.putString(MapFragment.MAP_REQUEST.CATEGORY_NEARBY_KEY, MapFragment.MAP_REQUEST.CATEGORY_NEARBY_VALUES.HOTEL.toString());
                        break;
                    case R.id.barPubImageButton:
                    case R.id.barPubTextView:
                        bundle.putString(MapFragment.MAP_REQUEST.TYPE_KEY, MapFragment.MAP_REQUEST.TYPE_VALUES.NEARBY_SEARCH.toString());
                        bundle.putString(MapFragment.MAP_REQUEST.CATEGORY_NEARBY_KEY, MapFragment.MAP_REQUEST.CATEGORY_NEARBY_VALUES.BARANDPUB.toString());
                        break;
                    case R.id.attractionImageButton:
                    case R.id.attractionTextView:
                        bundle.putString(MapFragment.MAP_REQUEST.TYPE_KEY, MapFragment.MAP_REQUEST.TYPE_VALUES.NEARBY_SEARCH.toString());
                        bundle.putString(MapFragment.MAP_REQUEST.CATEGORY_NEARBY_KEY, MapFragment.MAP_REQUEST.CATEGORY_NEARBY_VALUES.ATTRACTION.toString());
                        break;
                    case R.id.museumImageButton:
                    case R.id.museumTextView:
                        bundle.putString(MapFragment.MAP_REQUEST.TYPE_KEY, MapFragment.MAP_REQUEST.TYPE_VALUES.NEARBY_SEARCH.toString());
                        bundle.putString(MapFragment.MAP_REQUEST.CATEGORY_NEARBY_KEY, MapFragment.MAP_REQUEST.CATEGORY_NEARBY_VALUES.MUSEUM.toString());
                        break;
                    case R.id.bancomatImageButton:
                    case R.id.bancomatTextView:
                        bundle.putString(MapFragment.MAP_REQUEST.TYPE_KEY, MapFragment.MAP_REQUEST.TYPE_VALUES.NEARBY_SEARCH.toString());
                        bundle.putString(MapFragment.MAP_REQUEST.CATEGORY_NEARBY_KEY, MapFragment.MAP_REQUEST.CATEGORY_NEARBY_VALUES.BANCOMAT.toString());
                        break;
                }
                Navigation.findNavController(view).navigate(R.id.action_homeFragment_to_mapFragment, bundle);
            }
        };

        binding.restaurantImageButton.setOnClickListener(categoriesOnClick);
        binding.restaurantTextView.setOnClickListener(categoriesOnClick);
        binding.hotelImageButton.setOnClickListener(categoriesOnClick);
        binding.hotelTextView.setOnClickListener(categoriesOnClick);
        binding.barPubImageButton.setOnClickListener(categoriesOnClick);
        binding.barPubTextView.setOnClickListener(categoriesOnClick);
        binding.attractionImageButton.setOnClickListener(categoriesOnClick);
        binding.attractionTextView.setOnClickListener(categoriesOnClick);
        binding.museumImageButton.setOnClickListener(categoriesOnClick);
        binding.museumTextView.setOnClickListener(categoriesOnClick);
        binding.bancomatImageButton.setOnClickListener(categoriesOnClick);
        binding.bancomatTextView.setOnClickListener(categoriesOnClick);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());
        requireLocation(myLocationObserver);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d(TAG, "onActivityResult: request "+requestCode+", result "+resultCode);
        super.onActivityResult(requestCode, resultCode, data);

        // If the GPS is enabled, show the position on the map
        if(resultCode == Activity.RESULT_OK) {
            if (requestCode == ENABLE_GPS_REQUEST_CODE) {
                getLastKnownLocation();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            // If the user grant the locatino permission and GPS is enabled, retrieve the position
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if(checkGPS()){
                    getLastKnownLocation();
                }
            }
        }
    }

    private void initializeAutocompleteSearchFragment() {
        // Initialize place API
        if (!Places.isInitialized()) {
            Places.initialize(getActivity().getApplicationContext(), ApiConstants.Keys.GOOGLE_MAPS_API_KEY);
        }

        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(
                Place.Field.ID, Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.d(TAG, "Place selected "+place.getId());
                Bundle bundle = new Bundle();
                bundle.putString(MapFragment.MAP_REQUEST.TYPE_KEY, MapFragment.MAP_REQUEST.TYPE_VALUES.SHOW_POINT_OF_INTEREST.toString());
                bundle.putString(MapFragment.MAP_REQUEST.POI_ID_KEY, place.getId());
                bundle.putParcelable(MapFragment.MAP_REQUEST.POI_LATLNG_KEY, place.getLatLng());
                Navigation.findNavController(binding.getRoot()).navigate(R.id.action_homeFragment_to_mapFragment, bundle);
            }

            @Override
            public void onError(Status status) {
                Log.i(TAG, "An error occurred: " + status);
            }
        });
    }

    private void requireLocation(Observer<LatLng> locationObserver){
        location = new MutableLiveData<>();
        location.observe(this, locationObserver);
        if(isGoogleServicesOk()){
            checkLocationPermissions();
        }
    }

    private boolean isGoogleServicesOk(){
        Log.d(TAG,"Google services check");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getContext());
        if(available == ConnectionResult.SUCCESS)
            return true;
        else {
            Snackbar.make(getView(), R.string.location_google_service_error, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    private void checkLocationPermissions(){
        if (isLocationPermissionGranted()) {
            if(checkGPS()){
                getLastKnownLocation();
            }
            // Se il GPS non è enabled viene chiamato l'altro metodo
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this.getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new MaterialAlertDialogBuilder(getActivity())
                        .setTitle(getString(R.string.map_fragment_location_request_title))
                        .setMessage(getString(R.string.map_fragment_location_request_message))
                        .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                            requireLocationPermission();
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            } else {
                // Permission to access the location is missing. Show rationale and request permission
                requireLocationPermission();
            }
        }
    }

    private boolean isLocationPermissionGranted(){
        return ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requireLocationPermission(){
        requestPermissions(
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
    }

    private boolean checkGPS(){
        if(isGpsEnabled())
            return true;

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(1500);
        locationRequest.setFastestInterval(500);
        locationRequest.setNumUpdates(1);
        locationRequest.setSmallestDisplacement(10);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        MutableLiveData<LatLng> loc = this.location;
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null){
                    Location resultLocation = locationResult.getLocations().get(0);
                    loc.postValue(new LatLng(resultLocation.getLatitude(), resultLocation.getLongitude()));
                }
            }
        }, Looper.getMainLooper());

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(false);

        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(getContext())
                .checkLocationSettings(builder.build());

        task.addOnFailureListener(getActivity(), exception -> {
            Log.d(TAG, "isGpsEnabled - onFailureListener: exception "+exception.getMessage());

            if (exception instanceof ResolvableApiException) {
                try {
                    ResolvableApiException resolvable = (ResolvableApiException) exception;
                    startIntentSenderForResult(
                            resolvable.getResolution().getIntentSender(),
                            ENABLE_GPS_REQUEST_CODE,
                            null, 0,0 ,0,null);
                } catch (IntentSender.SendIntentException sendEx) {}
            }
        });
        return false;
    }

    private boolean isGpsEnabled(){
        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void getLastKnownLocation(){
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                Location location = task.getResult();
                if(location != null)
                    this.location.postValue(new LatLng(location.getLatitude(), location.getLongitude()));
                else {
                    this.location.postValue(null);
                }
            }
        });
    }

    private void getNearbyPlaces(double lat, double lon) {
        nearbySearchViewModel = new ViewModelProvider(requireActivity()).get(NearbySearchViewModel.class);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        binding.nearbyPlacesRecyclerView.setLayoutManager(layoutManager);

        //Mostro nearby places nella recyclerview usando adapter
        final Observer<Resource<List<NearbyPlace>>> observer = new Observer<Resource<List<NearbyPlace>>>() {
            @Override
            public void onChanged(Resource<List<NearbyPlace>> nearbyPlaces) {
                NearbyPlacesAdapter nearbyPlacesAdapter = new NearbyPlacesAdapter(getActivity(), nearbyPlaces.getData(), new NearbyPlacesAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(NearbyPlace nearbyPlace) {
                        final Observer<PointOfInterest> observer = new Observer<PointOfInterest>() {
                            @Override
                            public void onChanged(PointOfInterest poi) {
                                Intent intent = new Intent(getContext(), PointOfInterestActivity.class);
                                intent.putExtra(MiscCostants.INTENT_POI_KEY, poi);
                                startActivity(intent);
                            }
                        };
                        LiveData<PointOfInterest> liveData = new PointOfInterestViewModel().getPointOfInterest(nearbyPlace.getId());
                        liveData.observe(getViewLifecycleOwner(), observer);
                    }
                });
                binding.nearbyPlacesRecyclerView.setAdapter(nearbyPlacesAdapter);
            }
        };

        LiveData<Resource<List<NearbyPlace>>> liveData = nearbySearchViewModel.getNearbyPlaces(lat, lon);
        liveData.observe(getViewLifecycleOwner(), observer);
    }

}