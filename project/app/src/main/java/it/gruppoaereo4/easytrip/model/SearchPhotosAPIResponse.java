package it.gruppoaereo4.easytrip.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import it.gruppoaereo4.easytrip.model.pointofinterest.PhotoImageReference;

public class SearchPhotosAPIResponse {

    @SerializedName("items")
    private List<PhotoImageReference> photos;

    public List<PhotoImageReference> getPhotos() {
        return photos;
    }
}
