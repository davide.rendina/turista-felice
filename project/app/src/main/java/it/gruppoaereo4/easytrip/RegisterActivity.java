package it.gruppoaereo4.easytrip;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

import it.gruppoaereo4.easytrip.databinding.ActivityRegisterBinding;
import it.gruppoaereo4.easytrip.dbsync.SyncHandler;
import it.gruppoaereo4.easytrip.dbsync.UploadWorker;

public class RegisterActivity extends AppCompatActivity {

    private ActivityRegisterBinding binding;
    private Toolbar toolbar;
    private String TAG = "RegisterActivity";
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        firebaseAuth = FirebaseAuth.getInstance();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(button -> onBackPressed());

        binding.registerActivityRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(checkInputs()){ //empty fields
                    Snackbar.make(view,R.string.missing_input_snackbar,Snackbar.LENGTH_SHORT).show();
                } else {
                    String email = binding.registerActivityEmailEditText.getText().toString();
                    String password = binding.registerActivityPswEditText.getText().toString();
                    //using firebase method to create user from email & psw
                    firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Snackbar.make(view,R.string.register_activity_registration_success,Snackbar.LENGTH_SHORT).show();
                                SyncHandler.uploadAndDownload(getApplicationContext()); //sync favourites and trips
                                finish();
                            } else {
                                Log.d(TAG,"Task"+task.getException().getMessage());
                                Snackbar.make(view,"Error: "+task.getException().getMessage(),Snackbar.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
    }

    private boolean checkInputs(){
        return binding.registerActivityEmailEditText.getText().toString().matches("") ||
                binding.registerActivityPswEditText.getText().toString().matches("");
    }
}
