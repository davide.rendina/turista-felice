package it.gruppoaereo4.easytrip;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

import it.gruppoaereo4.easytrip.databinding.ActivityRecoverPasswordBinding;

public class RecoverPasswordActivity extends AppCompatActivity {

    private ActivityRecoverPasswordBinding binding;
    private FirebaseAuth firebaseAuth;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRecoverPasswordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        firebaseAuth = FirebaseAuth.getInstance();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(button -> onBackPressed());

        Intent intent = getIntent();
        if(intent.getStringExtra("email")!=null){
            binding.recoverActivityEmailEditText.setText(intent.getStringExtra("email"));
        }

        binding.recoverActivityLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(binding.recoverActivityEmailEditText.getText().toString().matches("")){
                    Snackbar.make(view, R.string.missing_input_snackbar,Snackbar.LENGTH_SHORT).show();
                } else {
                  String email = binding.recoverActivityEmailEditText.getText().toString();
                  //using firebase password recovery method
                  firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(RecoverPasswordActivity.this, new OnCompleteListener<Void>() {
                      @Override
                      public void onComplete(@NonNull Task<Void> task) {
                          if(task.isSuccessful()){
                              Snackbar.make(view,R.string.recover_activity_mail_success,Snackbar.LENGTH_LONG).show();
                          } else {
                              Snackbar.make(view,"Error: "+task.getException().getMessage(),Snackbar.LENGTH_LONG).show();
                          }
                      }
                  });
                }
            }
        });
    }
}
