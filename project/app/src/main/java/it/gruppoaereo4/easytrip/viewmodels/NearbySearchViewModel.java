package it.gruppoaereo4.easytrip.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import java.util.List;

import it.gruppoaereo4.easytrip.model.NearbyPlace;
import it.gruppoaereo4.easytrip.model.Resource;
import it.gruppoaereo4.easytrip.repositories.NearbySearchRepository;

public class NearbySearchViewModel extends ViewModel {

    private final String TAG = "NearbySearchViewModel";

    private double lastLat = 0;
    private double lastLon = 0;
    private int lastRadius = -1;
    private String lastType = null;

    private MutableLiveData<Resource<List<NearbyPlace>>> nearbyPlaces;

    //Metodo per recupare i place in home
    public LiveData<Resource<List<NearbyPlace>>> getNearbyPlaces(double lat, double lon){
        if (nearbyPlaces == null || !isLastCall(lat, lon)) {
            lastLat = lat;
            lastLon = lon;

            nearbyPlaces = new MutableLiveData<>();
            NearbySearchRepository.getInstance().getNearbyPlaces(nearbyPlaces, lat, lon, "what to do");
        }
        return nearbyPlaces;
    }

    public LiveData<Resource<List<NearbyPlace>>> getNearbyPlaces(double lat, double lon, int radius, String type){
        if (nearbyPlaces == null || ! isLastCall(lat, lon, radius, type)) {
            lastLat = lat;
            lastLon = lon;
            lastRadius = radius;
            lastType = type;

            nearbyPlaces = new MutableLiveData<>();
            NearbySearchRepository.getInstance().getNearbyPlaces(nearbyPlaces, type, lat, lon, radius, "");
        }
        return nearbyPlaces;
    }

    // Return True if the current call is equals to the last one (the one saved in this ViewModel)
    private boolean isLastCall(double lat, double lon, int radius, String type){
        return lastRadius == radius && lastType.equals(type) && isLastCall(lat, lon);
    }

    private boolean isLastCall(double lat, double lon){
        return lat == lastLat && lon == lastLon;
    }

}
