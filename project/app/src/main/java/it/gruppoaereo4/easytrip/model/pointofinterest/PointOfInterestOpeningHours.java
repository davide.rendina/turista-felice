package it.gruppoaereo4.easytrip.model.pointofinterest;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;

// Models opening hours for a pointOfInterest with Places API
public class PointOfInterestOpeningHours implements Parcelable {

    @SerializedName("weekday_text")
    private String[] timeArray; //Starts from Monday

    /* Matrix with a row for each days of week.
       The first column contains the day name, the second the time for that day.*/
    private String[][] timeTable;


    public String[][] getTimeTable(){
        if(timeTable == null)
            generateTimeTable();
        return timeTable;
    }

    // Generate the matrix timetable starting from timeArray returned from Places API
    private void generateTimeTable() {
        if(timeArray != null){
            timeTable = new String[7][2];
            for(int i = 0; i< timeTable.length; i++){
                String[] dayTime = timeArray[i].split(":", 2);
                timeTable[i][0] = dayTime[0].substring(0,1).toUpperCase() + dayTime[0].substring(1);
                timeTable[i][1] = dayTime[1];
            }
        }
        else
            timeTable = null;
    }

    // Get today opening hour
    public String getToday() {
        if(timeTable == null)
            generateTimeTable();
        int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) -1;
        return timeTable == null ? null :
                timeArray[dayOfWeek].substring(0,1).toUpperCase() + timeArray[dayOfWeek].substring(1);
    }


    protected PointOfInterestOpeningHours(Parcel in) {
        timeArray = in.createStringArray();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(timeArray);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PointOfInterestOpeningHours> CREATOR = new Creator<PointOfInterestOpeningHours>() {
        @Override
        public PointOfInterestOpeningHours createFromParcel(Parcel in) {
            return new PointOfInterestOpeningHours(in);
        }

        @Override
        public PointOfInterestOpeningHours[] newArray(int size) {
            return new PointOfInterestOpeningHours[size];
        }
    };


}
