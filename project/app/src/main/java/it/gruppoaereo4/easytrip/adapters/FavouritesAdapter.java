package it.gruppoaereo4.easytrip.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;

import java.util.List;

import it.gruppoaereo4.easytrip.R;
import it.gruppoaereo4.easytrip.model.pointofinterest.PointOfInterest;

public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesAdapter.FavouritesViewHolder> {

    private List<PointOfInterest> favouriteList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;

    public FavouritesAdapter(Context context, List<PointOfInterest> favouriteList, OnItemClickListener onItemClickListener){
        this.layoutInflater = LayoutInflater.from(context);
        this.favouriteList = favouriteList;
        this.onItemClickListener = onItemClickListener;
    }

    static class FavouritesViewHolder extends  RecyclerView.ViewHolder{
        TextView textViewTitle;
        ImageView imageView;
        TextView textViewType;

        public FavouritesViewHolder(View view){
            super(view);
            textViewTitle = view.findViewById(R.id.titleFavouriteTextView);
            imageView = view.findViewById(R.id.photoSmallPOIImageView);
            textViewType = view.findViewById(R.id.typeSmallPOITextView);
        }


        public void bind(PointOfInterest poi, OnItemClickListener onItemClickListener){
            //set the view
            textViewTitle.setText(poi.getName());
            Picasso.get().load(poi.getImageUrl()).into(imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClickListener(poi);
                }
            });
        }

    }

    public interface OnItemClickListener{
        void onItemClickListener(PointOfInterest pointOfInterest);
    }

    @NonNull
    @Override
    public FavouritesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.favourite_item,parent,false);
        return new FavouritesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouritesViewHolder holder, int position) {
        holder.bind(favouriteList.get(position), this.onItemClickListener);
    }

    @Override
    public int getItemCount() {
        if(favouriteList!=null)
            return favouriteList.size();
        else
            return 0;
    }

    public void setData(List<PointOfInterest> pointOfInterests){
        if(pointOfInterests!=null) {
            this.favouriteList = pointOfInterests;
            notifyDataSetChanged();
        }
    }
}
